/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
* Jan Olaf Blech
*  
* 2013,  2014, 2015
* 
*  Refactoring as of 05/02/2015 
*
*  Improvements for Spacio-temporal operators : Jan 2016
*
* BeSpaceD Core
*/

package BeSpaceDCore

import collection.mutable.HashMap;
import collection.mutable.HashSet;

import Log._

/*
 * 
 * Core Definitions
 * 
 */


class CoreDefinitions {
  
	// Combining parts of Invariants: adding a point and a segment coordinate wise
	def addPointSegment(p : Invariant, s : Invariant) : Invariant ={
	  (p,s) match {
	    case (OccupyPoint (xp, yp), OccupySegment (x1, y1, x2, y2, radius)) =>OccupySegment (x1 + xp, y1 + yp, x2 + xp, y2 + yp, radius)
	  }
	}
	
	
	//OccupyFreeBox to OccupyBox overapproximation
	def overapproximateFreeBox(box:Invariant) : Invariant ={
	  box match {
	    case (OccupyFreeBox(x1,y1,x2,y2,x3,y3,x4,y4)) => 
	      OccupyBox(
	          Math.min(Math.min(x1,x2),Math.min(x3,x4)),
	          Math.min(Math.min(y1,y2),Math.min(y3,y4)),
	          Math.max(Math.max(x1,x2),Math.max(x3,x4)),
	          Math.max(Math.max(y1,y2),Math.max(y3,y4)))
	    case i => i
	  }
	  
	}
  
	//general definition
  	def inclusionBox (b1 : Invariant , b2 : Invariant) : Boolean = {
	  (b1,b2) match {
	    case (OccupyBox (b1x1,b1y1,b1x2,b1y2),OccupyBox (b2x1,b2y1,b2x2,b2y2)) => 
	      (b1x1 >= b2x1) && (b1x2 <= b2x2) && (b1y1 >= b2y1) && (b1y2 <= b2y2)
	  }
	}
  	
  	//special case
	def inclusionBox (b1 : OccupyBox , b2 : OccupyBox) : Boolean ={
	  (b1,b2) match {
	    case (OccupyBox (b1x1,b1y1,b1x2,b1y2),OccupyBox (b2x1,b2y1,b2x2,b2y2)) => 
	      (b1x1 >= b2x1) && (b1x2 <= b2x2) && (b1y1 >= b2y1) && (b1y2 <= b2y2)
	  }
	}
	
	def prettyPrintSI (si : SI) : String ={
	  si match {
	    case (SI_Add (si1,si2)) => " ( "+prettyPrintSI(si1)+ " + " +prettyPrintSI(si1)+" ) "
	    case (SI_Sub (si1,si2)) => " ( "+prettyPrintSI(si1)+ " - " +prettyPrintSI(si1)+" ) "
	    case (SI_Times (si1,si2)) => " ( "+prettyPrintSI(si1)+ " * " +prettyPrintSI(si1)+" ) "
	    case (SI_C (c)) => c.toString
	    case (SI_Var (v)) => v.toString
	    case i => i.toString()
	  }
	}
	
	def OccupySegment2OccupyBoxes (inv : Invariant) : Invariant ={
	  inv match {
	    case (OccupySegment(x1,y1,x2,y2,r)) => 
	      BIGAND (
	        	OccupyBox(x1+((x2 - x1)*0.0).toInt,y1+((y2 - y1)*0.0).toInt,x1 + ((x2 - x1)*0.1).toInt,y1 + ((y2 - y1)*0.1).toInt):: 
	    		OccupyBox(x1+((x2 - x1)*0.1).toInt,y1+((y2 - y1)*0.1).toInt,x1 + ((x2 - x1)*0.2).toInt,y1+((y2 - y1)*0.2).toInt)::
	    		OccupyBox(x1+((x2 - x1)*0.2).toInt,y1+((y2 - y1)*0.2).toInt,x1 + ((x2 - x1)*0.3).toInt,y1+((y2 - y1)*0.3).toInt)::
	    		
	        	OccupyBox(x1+((x2-x1)*0.3).toInt,y1+((y2-y1)*0.3).toInt,x1 + ((x2-x1)*0.4).toInt,y1 + ((y2-y1)*0.4).toInt):: 
	    		OccupyBox(x1+((x2-x1)*0.4).toInt,y1+((y2-y1)*0.4).toInt,x1 + ((x2-x1)*0.5).toInt,y1 + ((y2-y1)*0.5).toInt)::
	    		OccupyBox(x1+((x2-x1)*0.5).toInt,y1+((y2-y1)*0.5).toInt,x1 + ((x2-x1)*0.6).toInt,y1 + ((y2-y1)*0.6).toInt)::
	    		
	        	OccupyBox(x1+((x2-x1)*0.6).toInt,y1+((y2-y1)*0.6).toInt,x1 + ((x2-x1)*0.7).toInt,y1 + ((y2-y1)*0.7).toInt):: 
	    		OccupyBox(x1+((x2-x1)*0.7).toInt,y1+((y2-y1)*0.7).toInt,x1 + ((x2-x1)*0.8).toInt,y1 + ((y2-y1)*0.8).toInt)::
	    		OccupyBox(x1+((x2-x1)*0.8).toInt,y1+((y2-y1)*0.8).toInt,x1 + ((x2-x1)*0.9).toInt,y1 + ((y2-y1)*0.9).toInt)::
	    		OccupyBox(x1+((x2-x1)*0.9).toInt,y1+((y2-y1)*0.9).toInt,x1 + ((x2-x1)*1.0).toInt,y1 + ((y2-y1)*1.0).toInt)::Nil
	    		)
	  }
	  
	}
	
	def OccupySegment2OccupyBoxes_Alt1 (inv : Invariant) : Invariant ={
	  inv match {
	    case (OccupySegment(x1,y1,x2,y2,r)) => 
	      	      //var xdiv : Int = x2 - x1;

	      BIGAND (
	        	OccupyBox(x1+((x2 - x1)*(0.0 - 0.1)).toInt,y1+((y2 - y1)*(0.0-0.1)).toInt,x1 + ((x2 - x1)*0.1).toInt,y1 + ((y2 - y1)*0.1).toInt):: 
	    		OccupyBox(x1+((x2 - x1)*0.0).toInt,y1+((y2 - y1)*0.0).toInt,x1 + ((x2 - x1)*0.2).toInt,y1+((y2 - y1)*0.2).toInt)::
	    		OccupyBox(x1+((x2 - x1)*0.1).toInt,y1+((y2 - y1)*0.1).toInt,x1 + ((x2 - x1)*0.3).toInt,y1+((y2 - y1)*0.3).toInt)::
	        	OccupyBox(x1+((x2-x1)*0.2).toInt,y1+((y2-y1)*0.2).toInt,x1 + ((x2-x1)*0.4).toInt,y1 + ((y2-y1)*0.4).toInt):: 
	    		OccupyBox(x1+((x2-x1)*0.3).toInt,y1+((y2-y1)*0.3).toInt,x1 + ((x2-x1)*0.5).toInt,y1 + ((y2-y1)*0.5).toInt)::
	    		OccupyBox(x1+((x2-x1)*0.4).toInt,y1+((y2-y1)*0.4).toInt,x1 + ((x2-x1)*0.6).toInt,y1 + ((y2-y1)*0.6).toInt)::
	        	OccupyBox(x1+((x2-x1)*0.5).toInt,y1+((y2-y1)*0.5).toInt,x1 + ((x2-x1)*0.7).toInt,y1 + ((y2-y1)*0.7).toInt):: 
	    		OccupyBox(x1+((x2-x1)*0.6).toInt,y1+((y2-y1)*0.6).toInt,x1 + ((x2-x1)*0.8).toInt,y1 + ((y2-y1)*0.8).toInt)::
	    		OccupyBox(x1+((x2-x1)*0.7).toInt,y1+((y2-y1)*0.7).toInt,x1 + ((x2-x1)*0.9).toInt,y1 + ((y2-y1)*0.9).toInt)::
	    		OccupyBox(x1+((x2-x1)*0.8).toInt,y1+((y2-y1)*0.8).toInt,x1 + ((x2-x1)*1.0).toInt,y1 + ((y2-y1)*1.0).toInt)::
	    		OccupyBox(x1+((x2-x1)*0.9).toInt,y1+((y2-y1)*0.9).toInt,x1 + ((x2-x1)*1.1).toInt,y1 + ((y2-y1)*1.1).toInt)::Nil

	    		)
	  }
	  
	}
	
	def OccupySegment2OccupyBoxes_Alt2 (inv : Invariant) : Invariant ={
	  inv match {
	    case (OccupySegment(x1,y1,x2,y2,r)) => 
	      	      //var xdiv : Int = x2 - x1;

	      BIGAND (
	        	OccupyBox(x1+((x2 - x1)*(0.0)).toInt-r,y1+((y2 - y1)*(0.0)).toInt-r,x1 + ((x2 - x1)*0.0).toInt+r,y1 + ((y2 - y1)*0.0).toInt+r):: 
	    		OccupyBox(x1+((x2 - x1)*0.1).toInt-r,y1+((y2 - y1)*0.1).toInt-r,x1 + ((x2 - x1)*0.1).toInt+r,y1+((y2 - y1)*0.1).toInt+r)::
	    		OccupyBox(x1+((x2 - x1)*0.2).toInt-r,y1+((y2 - y1)*0.2).toInt-r,x1 + ((x2 - x1)*0.2).toInt+r,y1+((y2 - y1)*0.2).toInt+r)::
	        	OccupyBox(x1+((x2-x1)*0.3).toInt-r,y1+((y2-y1)*0.3).toInt-r,x1 + ((x2-x1)*0.3).toInt+r,y1 + ((y2-y1)*0.3).toInt+r):: 
	    		OccupyBox(x1+((x2-x1)*0.4).toInt-r,y1+((y2-y1)*0.4).toInt-r,x1 + ((x2-x1)*0.4).toInt+r,y1 + ((y2-y1)*0.4).toInt+r)::
	    		OccupyBox(x1+((x2-x1)*0.5).toInt-r,y1+((y2-y1)*0.5).toInt-r,x1 + ((x2-x1)*0.5).toInt+r,y1 + ((y2-y1)*0.5).toInt+r)::
	        	OccupyBox(x1+((x2-x1)*0.6).toInt-r,y1+((y2-y1)*0.6).toInt-r,x1 + ((x2-x1)*0.6).toInt+r,y1 + ((y2-y1)*0.6).toInt+r):: 
	    		OccupyBox(x1+((x2-x1)*0.7).toInt-r,y1+((y2-y1)*0.7).toInt-r,x1 + ((x2-x1)*0.7).toInt+r,y1 + ((y2-y1)*0.7).toInt+r)::
	    		OccupyBox(x1+((x2-x1)*0.8).toInt-r,y1+((y2-y1)*0.8).toInt-r,x1 + ((x2-x1)*0.8).toInt+r,y1 + ((y2-y1)*0.8).toInt+r)::
	    		OccupyBox(x1+((x2-x1)*0.9).toInt-r,y1+((y2-y1)*0.9).toInt-r,x1 + ((x2-x1)*0.9).toInt+r,y1 + ((y2-y1)*0.9).toInt+r)::
	    		OccupyBox(x1+((x2-x1)*1.0).toInt-r,y1+((y2-y1)*1.0).toInt-r,x1 + ((x2-x1)*1.0).toInt+r,y1 + ((y2-y1)*1.0).toInt+r)::Nil
	    		)
	  }
	  
	}
	
	// The 3D box abstraction, needs testing 
	
	def OccupySegment2OccupyBoxes3D (inv : Invariant) : Invariant ={
	  inv match {
	    case (OccupySegment3D(x1,y1,z1,x2,y2,z2,r)) => 
	      	      //var xdiv : Int = x2 - x1;

	      BIGAND (
	        	Occupy3DBox(x1+((x2 - x1)*(0.0)).toInt-r,y1+((y2 - y1)*(0.0)).toInt-r,z1+((z2 - z1)*(0.0)).toInt-r,x1 + ((x2 - x1)*0.0).toInt+r,y1 + ((y2 - y1)*0.0).toInt+r,z1 + ((z2 - z1)*0.0).toInt+r):: 
	    		Occupy3DBox(x1+((x2 - x1)*0.1).toInt-r,y1+((y2 - y1)*0.1).toInt-r,z1+((z2 - z1)*(0.1)).toInt-r,x1 + ((x2 - x1)*0.1).toInt+r,y1+((y2 - y1)*0.1).toInt+r,z1 + ((z2 - z1)*0.1).toInt+r)::
	    		Occupy3DBox(x1+((x2 - x1)*0.2).toInt-r,y1+((y2 - y1)*0.2).toInt-r,z1+((z2 - z1)*(0.2)).toInt-r,x1 + ((x2 - x1)*0.2).toInt+r,y1+((y2 - y1)*0.2).toInt+r,z1 + ((z2 - z1)*0.2).toInt+r)::
	        	Occupy3DBox(x1+((x2-x1)*0.3).toInt-r,y1+((y2-y1)*0.3).toInt-r,z1+((z2 - z1)*(0.3)).toInt-r,x1 + ((x2-x1)*0.3).toInt+r,y1 + ((y2-y1)*0.3).toInt+r,z1 + ((z2 - z1)*0.3).toInt+r):: 
	    		Occupy3DBox(x1+((x2-x1)*0.4).toInt-r,y1+((y2-y1)*0.4).toInt-r,z1+((z2 - z1)*(0.4)).toInt-r,x1 + ((x2-x1)*0.4).toInt+r,y1 + ((y2-y1)*0.4).toInt+r,z1 + ((z2 - z1)*0.4).toInt+r)::
	    		Occupy3DBox(x1+((x2-x1)*0.5).toInt-r,y1+((y2-y1)*0.5).toInt-r,z1+((z2 - z1)*(0.5)).toInt-r,x1 + ((x2-x1)*0.5).toInt+r,y1 + ((y2-y1)*0.5).toInt+r,z1 + ((z2 - z1)*0.5).toInt+r)::
	        	Occupy3DBox(x1+((x2-x1)*0.6).toInt-r,y1+((y2-y1)*0.6).toInt-r,z1+((z2 - z1)*(0.6)).toInt-r,x1 + ((x2-x1)*0.6).toInt+r,y1 + ((y2-y1)*0.6).toInt+r,z1 + ((z2 - z1)*0.6).toInt+r):: 
	    		Occupy3DBox(x1+((x2-x1)*0.7).toInt-r,y1+((y2-y1)*0.7).toInt-r,z1+((z2 - z1)*(0.7)).toInt-r,x1 + ((x2-x1)*0.7).toInt+r,y1 + ((y2-y1)*0.7).toInt+r,z1 + ((z2 - z1)*0.7).toInt+r)::
	    		Occupy3DBox(x1+((x2-x1)*0.8).toInt-r,y1+((y2-y1)*0.8).toInt-r,z1+((z2 - z1)*(0.8)).toInt-r,x1 + ((x2-x1)*0.8).toInt+r,y1 + ((y2-y1)*0.8).toInt+r,z1 + ((z2 - z1)*0.8).toInt+r)::
	    		Occupy3DBox(x1+((x2-x1)*0.9).toInt-r,y1+((y2-y1)*0.9).toInt-r,z1+((z2 - z1)*(0.9)).toInt-r,x1 + ((x2-x1)*0.9).toInt+r,y1 + ((y2-y1)*0.9).toInt+r,z1 + ((z2 - z1)*0.9).toInt+r)::
	    		Occupy3DBox(x1+((x2-x1)*1.0).toInt-r,y1+((y2-y1)*1.0).toInt-r,z1+((z2 - z1)*(1.0)).toInt-r,x1 + ((x2-x1)*1.0).toInt+r,y1 + ((y2-y1)*1.0).toInt+r,z1 + ((z2 - z1)*1.0).toInt+r)::Nil
	    		)
	  }
	  
	}
	
  
	def prettyPrintInvariant (inv : Invariant) : String ={
	  inv match {
	    case EROccupyBox(x1,y1,x2,y2) => "EROccupyBox ( "+x1+" , " + y1 + " , " + x2 + " , " + y2 + " )"; // check if this works
	    case OccupyBoxSI(x1,y1,x2,y2) => "OccupyBoxSI ( "+prettyPrintSI(x1)+" , " + prettyPrintSI(y1) + " , " + prettyPrintSI(x2) + " , " + prettyPrintSI(y2) + " )";
	  	case OccupyBox(x1,y1,x2,y2) => "OccupyBox ( "+x1+" , " + y1 + " , " + x2 + " , " + y2 + " )";
	  	case OccupyBoxDouble(x1,y1,x2,y2) => "OccupyBox ( "+x1+" , " + y1 + " , " + x2 + " , " + y2 + " )";
	  	case OwnBox(c,x1,y1,x2,y2) => "OwnBox ["+ c + "] ( "+x1+" , " + y1 + " , " + x2 + " , " + y2 + " )";
	  	case OccupyPoint(x,y) => "OccupyPoint ( " + x + " , " + y + " )";
	  	case OccupyPointDouble(x,y) => "OccupyPoint ( " + x + " , " + y + " )";
	  	case OwnPoint(c,x,y) => "OwnPoint ["+c+"] ( " + x + " , " + y + " )";
	  	
	  	case Occupy3DBox(x1,y1,z1,x2,y2,z2) => "Occupy3DBox ( "+x1+" , " + y1 + " , " + z1 + " , " + x2 + " , " + y2 + " , " + z2 + " )";
	  	case Occupy3DBoxDouble(x1,y1,z1,x2,y2,z2) => "Occupy3DBox ( "+x1+" , " + y1 + " , " + z1 + " , " + x2 + " , " + y2 + " , " + z2 + " )";
	  	case Occupy3DPoint(x,y,z) => "OccupyPoint ( " + x + " , " + y + " , " + z +  ")";
	  	case Occupy3DPointDouble(x,y,z) => "OccupyPoint ( " + x + " , " + y + " , " + z +  ")";
	  	
	  	case OccupySegment (x1,y1,x2,y2,radius) => "OccupySegment ( " + x1 + " , " + y1 + " , " + x2 + " , " + y2 + " ; " + radius  + " )"; 
	  	case OwnSegment (c,x1,y1,x2,y2,radius) => "OwnSegment ["+ c + "] ( " + x1 + " , " + y1 + " , " + x2 + " , " + y2 + " ; " + radius  + " )"; 
	  	
	  	case OR (t1,t2) => "( " + prettyPrintInvariant(t1) + "  \\/  " + prettyPrintInvariant(t2) + " )";
	  	case AND (t1,t2) => "( " + prettyPrintInvariant(t1) + " /\\  " + prettyPrintInvariant(t2) + " )";
	  	case NOT (t) => "! ( " + prettyPrintInvariant(t) + " )";
	  	case IMPLIES(t1,t2) => "( " + prettyPrintInvariant(t1) + "  -->  " + prettyPrintInvariant(t2) + " )";
	  	case BIGOR (l) => 
	  	  var s : String = ""; 
	  	  for (e <- l) {
	  	    s +=prettyPrintInvariant(e);
	  	    } 
	  	  return(" \\/ : "+ s);
	  	case BIGAND (l) => 
	  	  var s : String = ""; 
	  	  for (e <- l) {
	  	    s +=prettyPrintInvariant(e);
	  	    } 
	  	  return(" /\\ : "+ s);
	  	  
	  	case IntTimeStamp (timestamp) => "TIME ( " + timestamp + " )";
	  	case TimeStamp (timestamp) => "TIME ( " + timestamp + " )";
	  	case TimePeriod (timestamp1, timestamp2) => "TIME [ " + timestamp1 + " ; " + timestamp2 + " ]";
	  	case TimePoint (timepoint) => "TIME ( " + timepoint + " )"; 
	  	case TimeInterval (timepoint1, timepoint2) => "TIME [ " + timepoint1 + " ; " + timepoint2 + " ]";
	  	case Event (e) => e.toString;
	  	case INSTATE (o,t,v) => s"AT $t ($o in state $v)";
	  	case Status (o,v) => s"($o in state $v)";
	  	
			case Owner(o) => "Owner(" + o.toString + ")"
			
	  	case Component (id) => s"Component(${id})"
	  	case ComponentState (s) => s"ComponentState(${s})"
	  	case ComponentDescription (d) => s"ComponentDescription(${d})"
	  	
	  	case OccupyNode (n) => "OccupyNode "+n;
	  	case OwnNode (c,n) => "OwnNODE [" + c + "] "+ n;
	  	case BetweenNodes (s,t) => "Between [ "+s+" ; "+t+" ]";
	  	
	  	case Edge(s,t) => "EDGE [ "+s+" ; "+t+" ]";
	  	case Transition(s,e,t) => "EDGE [ "+s+" -"+e+"-> "+t+" ]";
	  	case Value(v) => s"Value(${v})"
	  	
	  	case TRUE () => "True";
	  	case FALSE () => "False";
	  }
	} : String
  

	
	def invariantToJson (inv : Invariant) : String =
	{
	  inv match 
	  {
	    // Spatial and Ownership
	    case EROccupyBox(x1,y1,x2,y2)             => s"""{"type":"EROccupyBox","x1":"$x1","y1":"$y1","x2":"$x2","y2":"$y2"}"""
	    case OccupyBoxSI(x1,y1,x2,y2)             => s"""{"type":"OccupyBoxSI","x1":"${prettyPrintSI(x1)}","y1":"${prettyPrintSI(y1)}","x2":"${prettyPrintSI(x2)}","y2":"${prettyPrintSI(y2)}"}"""
	  	case OccupyBox(x1,y1,x2,y2)               => s"""{"type":"OccupyBox","x1":"$x1","y1":"$y1","x2":"$x2","y2":"$y2"}"""
	  	case OccupyBoxDouble(x1,y1,x2,y2)         => s"""{"type":"OccupyBoxDouble","x1":"$x1","y1":"$y1","x2":"$x2","y2":"$y2"}"""
	  	case OwnBox(c,x1,y1,x2,y2)                => s"""{"type":"OwnBox","owner":"$c","x1":"$x1","y1":"$y1","x2":"$x2","y2":"$y2"}"""
	  	
	  	case OccupyPoint(x,y)                     => s"""{"type":"OccupyPoint","x":"$x","y":"$y"}"""
	  	case OccupyPointDouble(x,y)               => s"""{"type":"OccupyPointDouble","x":"$x","y":"$y"}"""
	  	case OwnPoint(c,x,y)                      => s"""{"type":"OwnPoint","owner":"$c","x":"$x","y":"$y"}"""
	  	
	  	case Occupy3DBox(x1,y1,z1,x2,y2,z2)       => s"""{"type":"Occupy3DBox","x1":"$x1","y1":"$y1","z1":"$z1","x2":"$x2","y2":"$y2","z2":"$z2"}"""
	  	case Occupy3DBoxDouble(x1,y1,z1,x2,y2,z2) => s"""{"type":"Occupy3DBoxDouble","x1":"$x1","y1":"$y1","z1":"$z1","x2":"$x2","y2":"$y2","z2":"$z2"}"""
	  	case Occupy3DPoint(x,y,z)                 => s"""{"type":"Occupy3DPoint","x":"$x","y":"$y","z":"$z"}"""
	  	case Occupy3DPointDouble(x,y,z)           => s"""{"type":"Occupy3DPointDouble","x":"$x","y":"$y","z":"$z"}"""
	  	
	  	case OccupySegment (x1,y1,x2,y2,radius)   => s"""{"type":"OccupySegment","x1":"$x1","y1":"$y1","x2":"$x2","y2":"$y2","radius":"$radius"}"""
	  	case OwnSegment (c,x1,y1,x2,y2,radius)    => s"""{"type":"OwnSegment","owner":"$c","x1":"$x1","y1":"$y1","x2":"$x2","y2":"$y2","radius":"$radius"}"""
	  	
	    case OccupyNode (n)                       => s"""{"type":"OccupyNode","node":"$n"}"""
	  	case OwnNode (c,n)                        => s"""{"type":"OwnNode","owner":"$c","node":"$n"}"""
	  	case BetweenNodes (s,t)                   => s"""{"type":"BetweenNodes","owner":"$s","node":"$t"}"""

	    case Owner(c)                             => s"""{"type":"Owner","owner":"$c"}"""
	    
	  	// Logical
	  	case TRUE ()            =>  """"True""""
	  	case FALSE ()           =>  """"False""""
	  	case OR (t1,t2)         => s"""{"type":"OR","t1":${toJson(t1)},"t2":${toJson(t2)}}"""
	  	case AND (t1,t2)        => s"""{"type":"AND","t1":${toJson(t1)},"t2":${toJson(t2)}}"""
	  	case NOT (t)            => s"""{"type":"NOT","t":${toJson(t)}}}"""
	  	case IMPLIES(t1,t2)     => s"""{"type":"IMPLIES","t1":${toJson(t1)},"t2":${toJson(t2)}}"""
	  	case BIGOR (l) => 
	  	  {
	  	    val terms : String = l map { i: Invariant => toJson(i)} mkString ","
	  	    """{"type":"BIGOR","terms":[""" + terms + "]}"
	  	  }
	  	case BIGAND (l) =>
	  	  {
	  	    val terms : String = l map { i: Invariant => toJson(i)} mkString ","
	  	    """{"type":"BIGAND","terms":[""" + terms + "]}"
	      }
	  	  
	  	// Temporal
	  	case IntTimeStamp (timestamp)              => s"""{"type":"IntTimeStamp","timestamp":"$timestamp"}"""
	  	case TimeStamp (timestamp)                 => s"""{"type":"TimeStamp","timestamp":"$timestamp"}"""
	  	case TimePeriod (timestamp1, timestamp2)   => s"""{"type":"TimePeriod","timestamp1":"$timestamp1","timestamp2":"$timestamp2"}"""
	  	case TimePoint (timepoint)                 => s"""{"type":"TimePoint","timestamp":"timepoint"}"""
	  	case TimeInterval (timepoint1, timepoint2) => s"""{"type":"TimeInterval","timestamp1":"$timepoint1","timestamp2":"$timepoint2"}"""
	  	
	  	case Event (e)                             => s"""{"type":"Event","event":"${e match { case inv:Invariant => toJson(inv); case _ => e.toString}}"}"""
	    case INSTATE(o,t,v)                        => s"""{"type":"INSTATE","owner":"$o","timepoint":"$t","value":"$v"}"""
	    case Status(o,v)                           => s"""{"type":"Status","owner":"$o","value":"$v"}"""
	  				
			// Structural
	  	case Component (id)           => s"""{"type":"Component","id":"$id"}"""
	  	case ComponentState (s)       => s"""{"type":"ComponentState","state":"$s"}"""
	  	case ComponentDescription (d) => s"""{"type":"ComponentDescription","state":"$d"}"""
	  	
	  	case Edge(s,t)                => s"""{"type":"Edge","source":"$s","target":"$t"}"""
	  	case Transition(s,e,t)        => s"""{"type":"Transition","source":"$s","event":"$e","target":"$t"}"""

	    case Value(v)                 => s"""{"type":"Value","value":"$v"}"""

	  	// Non Case Classes
	  	case other => 
	  	  {
	  	    if (other.isInstanceOf[EdgeAnnotated[Any,Any]])
	  	    {
	  	      val edge = other.asInstanceOf[EdgeAnnotated[Any,Any]]
	  	      edgeToJson(edge)
	  	    }
	  	    else if (other.isInstanceOf[BeGraphAnnotated[Any,Any]])
	  	    {
	  	      val graph = other.asInstanceOf[BeGraphAnnotated[Any,Any]]
	  	      graphToJson(graph)
	  	    }
	  	    else
	  	      s"""{"unknown":"${other.toString}""}""" 
	      }
    }
	}
	  
  def toJson(any: Any): String =
  {
    if (any.isInstanceOf[Invariant])
    {
      invariantToJson(any.asInstanceOf[Invariant])
    }
    else if(any.isInstanceOf[Jsonable])
    {
      any.asInstanceOf[Jsonable].toJson
    }
    else
    {
      any.toString
    }
  }
	
  

	
	/* JO: modification history:
	 * unfolding of a circle into points : added 09/04/2014
	 */
	
	def unfoldCircle (inv : Invariant) : Invariant ={
	  var pointset  = new HashSet[(Int,Int)]();
	  //create points
	  inv match {
	  	case OccupyCircle(x,y,r) => 
	  	  for (d :Double <- 0.0 to (Math.PI * 2.0) by 0.005) {
	  	  for ( i <- 0 to r) {
	  		  pointset.add(((x.doubleValue + (Math.sin(d) * i.toDouble)).intValue(),(y.doubleValue + (Math.cos(d) * i.toDouble)).intValue()))
	  	  }
	  	}
	  	case _ => 
	  }
	  
	  //extract points from map:
	  var retlist : List[Invariant] = Nil;
	  for ((i,j) <- pointset) {
	    retlist ::= OccupyPoint(i,j)
	  }
	  
	  return BIGAND(retlist);
	}

	// alternative definition may be faster
	// added 12/05/2014
	// untested as of 12/05/2014; tested 15/05/2014 -> ok 
	def unfoldCircleAlt1 (inv : Invariant) : Invariant ={
	  var pointset  = new HashSet[(Int,Int)]();
	  //create points
	  inv match {
	  	case OccupyCircle(x,y,r) => 
	  	  for ( i <- -r to r) {
	  	    for (j <- -r to r) {
	  	      if ((Math.sqrt(i.toDouble *i.toDouble + j.toDouble *j.toDouble)) <=r.toDouble) {
	  	        pointset.add(x+i,y+j)
	  	      }
	  	    }
	  	  }
	  	  for (d :Double <- 0.0 to (Math.PI * 2.0) by 0.005) {
	  	  for ( i <- 0 to r) {
	  		  pointset.add(((x.doubleValue + (Math.sin(d) * i.toDouble)).intValue(),(y.doubleValue + (Math.cos(d) * i.toDouble)).intValue()))
	  	  }
	  	}
	  	case _ => 
	  }
	  
	  //extract points from map:
	  var retlist : List[Invariant] = Nil;
	  for ((i,j) <- pointset) {
	    retlist ::= OccupyPoint(i,j)
	  }
	  
	  return BIGAND(retlist);
	}

	/*
	 * JO: modification history:
	 * BIGAND unfold added 23/01/2014F
	 */
	def unfoldInvariant (inv : Invariant) : Invariant ={
	  inv match {
	    case BIGAND (list) => BIGAND (list.map(unfoldInvariant))
	  	case OccupyBox(x1,y1,x2,y2) =>
			{
				if (x1 < x2) AND(unfoldInvariant(OccupyBox(x1, y1, x1, y2)), unfoldInvariant(OccupyBox(x1 + 1, y1, x2, y2)))
				else if (y1 <= y2) AND(OccupyPoint(x1, y1), unfoldInvariant(OccupyBox(x1, y1 + 1, x2, y2)))
				else TRUE()
			}
	  	//untested
	  	case Occupy3DBox(x1,y1,z1,x2,y2,z2) =>
	  	  								if (x1 < x2)  AND (unfoldInvariant(Occupy3DBox(x1,y1,z1,x1,y2,z1)),unfoldInvariant(Occupy3DBox(x1 + 1,y1,z1,x2,y2,z2)))
	  	  								else if (y1 < y2)  AND (unfoldInvariant(Occupy3DBox(x1,y1,z1,x1,y1,z2)),unfoldInvariant(Occupy3DBox(x1,y1+1,z1,x1,y2,z2)))
	  									else if (z1 <= z2) AND (Occupy3DPoint (x1,y1,z1),unfoldInvariant(Occupy3DBox(x1,y1,z1+1,x2,y2,z2)))
	  									else TRUE ();
	    case OwnBox(c,x1,y1,x2,y2) => if (x1 < x2)  AND (unfoldInvariant(OwnBox(c,x1,y1,x1,y2)),unfoldInvariant(OwnBox(c,x1 + 1,y1,x2,y2)))
	  									else if (y1 <= y2) AND (OwnPoint (c,x1,y1),unfoldInvariant(OwnBox(c,x1,y1+1,x2,y2)))
	  									else TRUE ();
	    case OccupyCircle(x,y,r) => unfoldCircleAlt1(inv : Invariant)
	  	case OccupyPoint(x,y) => OccupyPoint(x,y);
	  	case OwnPoint(c,x,y) => OwnPoint(c,x,y); // IMPLIES(Owner(c),OccupyPoint(x,y))
	  	case INSTATE(o,t,v) => IMPLIES(AND(Owner(o),TimePoint(t)),Value(v)) 
	  	case OR (t1,t2) => OR (unfoldInvariant(t1),unfoldInvariant(t2));
	  	case AND (t1,t2) => AND (unfoldInvariant(t1),unfoldInvariant(t2));
	  	case NOT (t) => NOT (unfoldInvariant(t));
	  	case IMPLIES(t1,t2) => IMPLIES (unfoldInvariant(t1),unfoldInvariant(t2));
	  	case IntTimeStamp (timestamp) => IntTimeStamp(timestamp);
	  	case TimeStamp (timestamp) => TimeStamp(timestamp);
	  	case TRUE () => TRUE ();
	  	case FALSE () => FALSE ();
	  	case c => c;
	  }
	}

	/*
	 * KF: created based on unfoldInvariant
	 */
	def unfoldInvariant2(inv: Invariant): Invariant =
	{
		inv match
		{
			case p: OccupyPoint   => p
			case box: OccupyBox   => BIGAND( unfoldBox(box) )
			case box: OwnBox[Any] => BIGAND( unfoldBox(box) )

			case AND(t1, t2)   => flatten( AND( unfoldInvariant2(t1), unfoldInvariant2(t2) ) )
			case BIGAND(terms) => (terms map unfoldInvariant2) reduce { (i1: Invariant, i2: Invariant) => flatten(AND(i1, i2)) }

			case OR(t1, t2)    => flatten( OR( unfoldInvariant2(t1), unfoldInvariant2(t2) ) )
			case BIGOR(terms)  => (terms map unfoldInvariant2) reduce { (i1: Invariant, i2: Invariant) => flatten(OR(i1, i2)) }

			case IMPLIES(p,c)  => IMPLIES(p, unfoldInvariant2(c))
			// Note: We made a decision here not to unfold the premise, assuming it only makes sense to have occupations in the conclusion.
		}
	}

	def unfoldBox (box : OccupyBox) : List[Invariant] = unfoldBoxGeneric(box)
  def unfoldBox (box : OwnBox[Any]) : List[Invariant] = unfoldBoxGeneric(box)

	def unfoldBoxGeneric (invariant : Invariant) : List[OccupyPoint] =
	{
		val points: List[Invariant] =
			invariant match
			{
				case OccupyBox(x1,y1,x2,y2) =>
					if (x1 < x2)  unfoldBoxGeneric(OccupyBox(x1,y1,x1,y2)) ++ unfoldBoxGeneric(OccupyBox(x1 + 1,y1,x2,y2))
					else if (y1 <= y2) OccupyPoint(x1,y1) :: unfoldBoxGeneric(OccupyBox(x1,y1+1,x2,y2))
					else List(TRUE())

				//untested
				case OwnBox(c,x1,y1,x2,y2) =>
					if (x1 < x2)  unfoldBoxGeneric(OwnBox(c,x1,y1,x1,y2)) ++ unfoldBoxGeneric(OwnBox(c,x1 + 1,y1,x2,y2))
					else if (y1 <= y2) OwnPoint (c,x1,y1) :: unfoldBoxGeneric(OwnBox(c,x1,y1+1,x2,y2))
					else List(TRUE())

				case OccupyPoint(x,y) => List(OccupyPoint(x,y))

				case AND (t1,t2) => unfoldBoxGeneric(t1) ++ unfoldBoxGeneric(t2)

				case c => List(c)
			}

		val purePoints = (points filter { _.isInstanceOf[OccupyPoint] }).asInstanceOf[List[OccupyPoint]]

    purePoints.sorted(ord = null)
	}

	// This function merges two timestamped boxes into a time period box as defined below
	def mergeSubInvariantsTimePeriodWImplies (sinv1 : Invariant, sinv2 : Invariant) : Invariant ={
	  (sinv1,sinv2) match {
	  	case (IMPLIES (TimeStamp (t1),OccupyBox(b1x1,b1y1,b1x2,b1y2)),IMPLIES (TimeStamp (t2),OccupyBox(b2x1,b2y1,b2x2,b2y2))) => 
	  	  IMPLIES(TimePeriod(t1,t2),OccupyBox(math.min(b1x1,b2x1),math.min(b1y1,b2y1),math.max(b1x2,b2x2),math.max(b1y2,b2y2) ))
	  	case (IMPLIES (TimeStamp (t1),OwnBox(c,b1x1,b1y1,b1x2,b1y2)),IMPLIES (TimeStamp (t2),OwnBox(c2,b2x1,b2y1,b2x2,b2y2))) => 
	  	  	if (c.equals(c2)) // owners should be the same before merging
	  		  IMPLIES(TimePeriod(t1,t2),OwnBox(c,math.min(b1x1,b2x1),math.min(b1y1,b2y1),math.max(b1x2,b2x2),math.max(b1y2,b2y2) ));
	  	  	else
	  	  	  FALSE();
	  	case (i1,i2) => FALSE ()
	  }
	}
	
	// This function merges two timestamped boxes into a time period box as defined below

	def mergeSubInvariantsTimePeriod[T] (sinv1 : Invariant, sinv2 : Invariant, ts1 : T, ts2 : T) : Invariant ={
	  (sinv1,sinv2) match {
	  	case (OccupyBox(b1x1,b1y1,b1x2,b1y2),OccupyBox(b2x1,b2y1,b2x2,b2y2)) => 
	  	  IMPLIES(TimePeriod(ts1,ts2),OccupyBox(math.min(b1x1,b2x1),math.min(b1y1,b2y1),math.max(b1x2,b2x2),math.max(b1y2,b2y2) ))
	  	case (OwnBox(c,b1x1,b1y1,b1x2,b1y2),OwnBox(c2,b2x1,b2y1,b2x2,b2y2)) => 
	  	  if (c.equals(c2)) // owners should be the same before merging
	  	  IMPLIES(TimePeriod(ts1,ts2),OwnBox(c,math.min(b1x1,b2x1),math.min(b1y1,b2y1),math.max(b1x2,b2x2),math.max(b1y2,b2y2) ));
	  	  else
	  	    FALSE();
	  	case (i1,i2) => FALSE ()
	  }
	}



	// ------------------------------------------------- Generic Tree Walking and Processing

	type InvariantIMPLIES = IMPLIES[Invariant,Invariant]

	type Processor[I, O] = I => O
	type InvariantProcessor = Processor[Invariant, Invariant]
	type ListProcessor[I, O] = List[I] => List[O]
	type InvariantListProcessor = ListProcessor[Invariant, Invariant]

	def applyToLogicTerm(f: InvariantProcessor)(inv: Invariant): Invariant =
	{
    val apply = applyToImplicationTerm(f) _ compose applyToNotTerm(f) _ compose applyToJunctionTerm(f) _

		apply(inv)
	}


	// Junctions

	def applyToJunctionTerm(f: InvariantProcessor)(inv: Invariant): Invariant =
	{
		inv match {
			case AND(t1, t2)   => AND(f(t1), f(t2))
			case BIGAND(terms) => BIGAND(terms map f)
			case OR(t1, t2)    => OR(f(t1), f(t2))
			case BIGOR(terms)  => BIGOR(terms map f)
			case other         => other
		}
	}

	def applyToJunctionTermList[OUT <: Invariant](treeWalk: Boolean)(f: InvariantListProcessor)(inv: Invariant): OUT =
	{
		val apply = applyToDisjunctionTermList[OUT](treeWalk)(f) _ compose applyToConjunctionTermList[OUT](treeWalk)(f) _

		apply(inv)
	}

	def applyToDisjunctionTermList[OUT <: Invariant](treeWalk: Boolean)(f: InvariantListProcessor)(inv: Invariant): OUT =
	{
		val recurse = if (treeWalk) applyToDisjunctionTermList[Invariant](treeWalk = true)(f) _ else { x: Invariant => x }

		val result = inv match {
			// Apply f() and recurse
			case AND(t1, t2)   => BIGAND( f(List(recurse(t1), recurse(t2))) )
			case BIGAND(terms) => BIGAND( f(terms map recurse) )
			case OR(t1, t2)    => BIGOR( f(List(recurse(t1), recurse(t2))) )
			case BIGOR(terms)  => BIGOR( f(terms map recurse) )

			// Recurse only
			case NOT(t1)       => NOT(recurse(t1))
			case IMPLIES(p, c) => IMPLIES(recurse(p), recurse(c))

			// Pass through
			case other         =>  other
		}

		result.asInstanceOf[OUT]
	}
	
	def applyToConjunctionTermList[OUT <: Invariant](treeWalk: Boolean)(f: InvariantListProcessor)(inv: Invariant): OUT =
	{
		val recurse = if (treeWalk) applyToConjunctionTermList[Invariant](treeWalk = true)(f) _ else { x: Invariant => x }

		val result = inv match {
			// Apply f() and recurse
			case AND(t1, t2)   => BIGAND( f(List(recurse(t1), recurse(t2))) )
			case BIGAND(terms) => BIGAND( f(terms map recurse) )

			// Recurse only
			case OR(t1, t2)    => OR(recurse(t1), recurse(t2))
			case BIGOR(terms)  => BIGOR(terms map recurse)
			case NOT(t1)       => NOT(recurse(t1))
			case IMPLIES(p, c) => IMPLIES(recurse(p), recurse(c))

			// Pass through
			case other         =>  other
		}

		result.asInstanceOf[OUT]
	}


	// Junctions - full tree

	def applyToJunctionTermListAll[OUT <: Invariant]    = applyToJunctionTermList[OUT]    (treeWalk = true) _
	def applyToDisjunctionTermListAll[OUT <: Invariant] = applyToDisjunctionTermList[OUT] (treeWalk = true) _
	def applyToConjunctionTermListAll[OUT <: Invariant] = applyToConjunctionTermList[OUT] (treeWalk = true) _



	// Junctions - Top level junction only

	def applyToJunctionTermListTopOnly[OUT <: Invariant]    = applyToJunctionTermList[OUT]    (treeWalk = false) _
	def applyToDisjunctionTermListTopOnly[OUT <: Invariant] = applyToDisjunctionTermList[OUT] (treeWalk = false) _
	def applyToConjunctionTermListTopOnly[OUT <: Invariant] = applyToConjunctionTermList[OUT] (treeWalk = false) _



	// IMPLIES

	def applyToImplicationTerm(f: InvariantProcessor)(inv: Invariant): Invariant =
	{
		val apply = applyToPremise(f) _ compose applyToConclusion(f) _

		apply(inv)
	}

	def applyToPremise(f: InvariantProcessor)(inv: Invariant): Invariant =
	{
		val recurse = applyToPremise(f) _

		inv match {
			// Apply f() and recurse
			case IMPLIES(premise, conclusion) => IMPLIES(f(recurse(premise)), recurse(conclusion))

			// Recurse only
			case AND(t1, t2)   => AND(recurse(t1), recurse(t2))
			case BIGAND(terms) => BIGAND(terms map recurse)
			case OR(t1, t2)    => OR(recurse(t1), recurse(t2))
			case BIGOR(terms)  => BIGOR(terms map recurse)
			case NOT(t1)       => NOT(recurse(t1))

			// Pass through
			case other         => other
		}
	}

	def applyToConclusion(f: InvariantProcessor)(inv: Invariant): Invariant =
	{
		val recurse = applyToConclusion(f) _

		inv match {
			// Apply f() and recurse
			case IMPLIES(premise, conclusion) => IMPLIES(recurse(premise), f(recurse(conclusion)))

			// Recurse only
			case AND(t1, t2)   => AND(recurse(t1), recurse(t2))
			case BIGAND(terms) => BIGAND(terms map recurse)
			case OR(t1, t2)    => OR(recurse(t1), recurse(t2))
			case BIGOR(terms)  => BIGOR(terms map recurse)
			case NOT(t1)       => NOT(recurse(t1))

			// Pass through
			case other         => other
		}
	}




	// NOT

	def applyToNotTerm(f: InvariantProcessor)(inv: Invariant): Invariant =
	{
		val recurse = applyToNotTerm(f) _

		inv match {
			// Apply f() and recurse
			case NOT(t1)       => NOT(f(recurse(t1)))

			// Recurse only
			case AND(t1, t2)   => AND(recurse(t1), recurse(t2))
			case BIGAND(terms) => BIGAND(terms map recurse)
			case OR(t1, t2)    => OR(recurse(t1), recurse(t2))
			case BIGOR(terms)  => BIGOR(terms map recurse)
			case IMPLIES(premise, conclusion) => IMPLIES(recurse(premise), recurse(conclusion))

			// Pass through
			case other         => other
		}
	}



  // ---------------------------------------------------------------- Flattening

  val flatten: InvariantProcessor = applyToDisjunctionTermListAll[Invariant](flattenOrList) compose applyToConjunctionTermListAll[Invariant](flattenAndList)

	def flattenAndList(list: List[Invariant]): List[Invariant] =
	{
		def extractAndList(inv: Invariant): List[Invariant] =
		{
			inv match
			{
				case AND(t1, t2)   => List(t1, t2)
				case BIGAND(terms) => terms
				case other         => List(other)
			}
		}

		if (list == Nil) Nil
		else
		{
			val flatList = extractAndList(list.head)
			val len = flatList.length

			debugOff
			{
			  println(s"flattenAndList: flatList length = $len")
			  println(s"flattenAndList: flatList head = ${flatList.head}")
			  println(s"flattenAndList: flatList tail length = ${flatList.tail.length}")
			}
			
			flatList ++ flattenAndList(list.tail)
		}
	}

	def flattenOrList(list: List[Invariant]): List[Invariant] =
	{
		def extractOrList(inv: Invariant): List[Invariant] =
		{
			inv match
			{
				case OR(t1, t2)   => List(t1, t2)
				case BIGOR(terms) => terms
				case other        => List(other)
			}
		}

		if (list == Nil) Nil
		else
		{
			val flatList = extractOrList(list.head)

			flatList ++ flattenOrList(list.tail)
		}
	}



	def flattenNestedAndList(list: List[Invariant]): List[Invariant] =
	{
		if (list == Nil) Nil
		else
		{
			val flatList = list.head match
			{
				case AND(t1, t2)  => flattenNestedAndList(t1 :: Nil) ++ flattenNestedAndList(t2 :: Nil)
				case BIGAND(l1)   => flattenNestedAndList(l1)
				case item         => List(item)
			}

			flatList ++ flattenNestedAndList(list.tail)
		}
	}



	def flattenNestedOrList(list: List[Invariant]): List[Invariant] =
	{
		if (list == Nil) Nil
		else
		{
			val flatList = list.head match {
				case OR(t1, t2) => flattenNestedOrList(t1 :: Nil) ++ flattenNestedOrList(t2 :: Nil)
				case BIGOR(l1) => flattenNestedOrList(l1)
				case item => List(item)
			}

      flatList ++ flattenNestedOrList(list.tail)
		}
	}



	// ---------------------------------------------------------- Normalised Ordering

  val order = applyToJunctionTermListAll[Invariant](orderTermList)

  def orderTermList: InvariantListProcessor = _.sorted


	// ---------------------------------------------------------- De-duplication

  val dedupe = applyToJunctionTermListAll[Invariant](dedupeTermList)

	def dedupeTermList: InvariantListProcessor = _.distinct



	// ---------------------------------------------------------- Merge Implications with equal Premises

	// Note: There are two kinds of premise merges:
	// One where they appear in a disjunction and
	// another where they appear in a conjunction

	def mergeImplicationList[JunctionType, P <: Invariant]: ListProcessor[Invariant, IMPLIES[P, Invariant]] =
	{
		type I = IMPLIES[P, Invariant]
					
    def findImplicationWithPremise(premise: P, list: List[Invariant]): Option[I] =
		{
			def hasPremise(premise: Invariant, potentialImplication: Invariant): Boolean =
			{
				potentialImplication match
					{
					case IMPLIES(p,c) => p == premise
					case _ => false
				}
			}

			val found: Option[Invariant] = list find { inv => hasPremise(premise, inv) }

      found.asInstanceOf[Option[I]]
			// Note: We can cast the Invariant to IMPLIES here because we know hasPremise will only return true for IMPLIES.
		}

		junctionList: List[Invariant] =>
			debugOn(s"junctionList = $junctionList")

			if (junctionList == Nil) Nil
			else {
				val rawHead: Invariant = junctionList.head
				val mergedTail: List[I] = mergeImplicationList[JunctionType, P](junctionList.tail)

				val mergeResult: List[I] = rawHead match
				{
					case newImplication: I =>
					{
						val oldImplicationOption: Option[I] = findImplicationWithPremise(newImplication.premise, mergedTail)

						if (oldImplicationOption.isDefined) {
							val oldImplication: I = oldImplicationOption.get

							val mergedImplication = mergeImplications(newImplication, oldImplication)

							val strippedTail = mergedTail.filterNot(_ == oldImplication)

							mergedImplication :: strippedTail
						}
						else
						{
							newImplication :: mergedTail
						}
					}

					case other => mergedTail // Delete all terms that are not IMPLIES
				}

				debugOn(s"mergeResult = $mergeResult")

				mergeResult
			}
	}

	def mergeImplications[JunctionType, P <: Invariant](
																						 newImplication: IMPLIES[P,Invariant],
																						 oldImplication: IMPLIES[P,Invariant]
																						 ): IMPLIES[P,Invariant] =
	{
		// Merge the rawHead's conclusion with the found conclusion
		// Note: Merge depends on the junction type [JunctionType]

		val mergedConclusion: Invariant = if (BIGAND(Nil).isInstanceOf[JunctionType]) {
			// Conjunction
			AND(newImplication.conclusion, oldImplication.conclusion)
		}
		else {
			// Disjunction
			OR(newImplication.conclusion, oldImplication.conclusion)
		}

		val mergedImplication = IMPLIES[P, Invariant](newImplication.premise, mergedConclusion)

		mergedImplication
	}


	
	// ----- Junction Type Specific versions

	// Disjunction

	val mergeImplicationsDisjunct: InvariantProcessor = applyToJunctionTermListTopOnly(mergeImplicationList[BIGOR[Invariant], Invariant])

	// Conjunction

	val mergeImplicationsConjunct: InvariantProcessor = applyToJunctionTermListTopOnly(mergeImplicationList[BIGAND[Invariant], Invariant])

	// Conjunction - assuming BeMaps
	// Note: This is a special case that assumes the input has a conjunction of IMPLIES only and results in a BeMap.
	//       so we make sure its typed as such.
	//       non-IMPLIES in the input will be thrown away.

	def mergeBeMap[P <: Invariant](inv: Invariant): BeMap[P, Invariant] =
	{
		require(inv.isInstanceOf[AND] || inv.isInstanceOf[BIGAND[Invariant]], "The input invariant must be a conjunction (AND or BIGAND)")

		val bigand: BIGAND[Invariant] = inv match
		{
			case AND(t1, t2)           => BIGAND(List(t1, t2))
			case ba: BIGAND[Invariant] => ba
			case _                     => BIGAND(Nil)
		}

		val mergeFunction: ListProcessor[Invariant, IMPLIES[P, Invariant]] = mergeImplicationList[BIGAND[Invariant], P]

		val mergedResult: BIGAND[IMPLIES[P, Invariant]] = applyToConjunctionTermListTopOnly(mergeFunction)(bigand)

		debugOn(s"mergedResult = $mergedResult")

		val simplifiedList: List[IMPLIES[P, Invariant]] = simplifyAndList(mergedResult.terms)

		debugOff(s"simplifiedList = $simplifiedList")

		val mapResult = BeMap[P, Invariant](simplifiedList)

		debugOff(s"mapResult = $mapResult")

		mapResult
	}




	// Merge Owner Premises

	type OwnerBeMap = BeMap[Owner[Any],Invariant]

	/**
	 * Merges implications with identical premises into one implication.
	 * Note: The input need to be converted into a BeMap. It must be a conjuction and any terms that are not IMPLIES will be thrown away.
	 *
	 * Signature:
	 *      Invariant => OwnerBeMap
	 *
	 * Note: THis signature is compatible with Processor.
	 */
	val mergeOwners: Processor[Invariant, OwnerBeMap] =
		{
			type Entry = IMPLIES[Owner[Any], Invariant]

			inputInvariant: Invariant =>

				// Convert to BeMap
				val impliesList: List[Invariant] = inputInvariant match
				{
					case AND(t1, t2)   => List(t1, t2)
					case BIGAND(terms) => terms
					case _             => Nil
				}

				val entries = (impliesList filter { _.isInstanceOf[Entry]}).asInstanceOf[List[Entry]]

				val inputBeMap = BIGAND(entries)

				// Merge
				val outputInvariant: OwnerBeMap = mergeBeMap[Owner[Any]](inputBeMap)

				outputInvariant
		}

	// Merge TimePoint Premises
	//TODO

	// Merge Both : Timepoint --> Owner --> ???
	// TODO


	// ---------------------------------------------------------- Normalisation Transformers

	val simplify: InvariantProcessor = simplifyInvariant _

	val normalize: InvariantProcessor = flatten compose order compose dedupe compose simplify

  val normalizeOwnerOccupied: InvariantProcessor = mergeOwners compose flatten compose order compose dedupe compose simplify


	val normalizeTemporalOwnerOccupied: InvariantProcessor = mergeOwners compose flatten compose order compose dedupe compose simplify


	// ----------------------------------------------------------------- Simplification

	// not completed yet, may need to be applied multiple times until a fixpoint is reached
	def simplifyInvariant (inv: Invariant) : Invariant =
	{
		// Flatten ORs
		def flattenOr(t1: Invariant, t2: Invariant, t3: Invariant): Invariant = flattenBigOr(simplifyInvariant(t1), simplifyInvariant(t2) :: simplifyInvariant(t3) :: Nil)
		def flattenBigOr(t1: Invariant, sublist: List[Invariant]): Invariant =
		{
			val flatList = flattenNestedOrList(t1 :: sublist)
			BIGOR(flatList)
		}

		// Flatten ANDs
		def flattenAnd(t1: Invariant, t2: Invariant, t3: Invariant): Invariant = flattenBigAnd(simplifyInvariant(t1), simplifyInvariant(t2) :: simplifyInvariant(t3) :: Nil)
		def flattenBigAnd(t1: Invariant, sublist: List[Invariant]): Invariant =
		{
			val flatList = flattenNestedAndList(t1 :: sublist)
			BIGAND(flatList)
		}

		inv match
		{
			// OR
			case OR (t1, FALSE()) => simplifyInvariant(t1)
	    case OR (FALSE(),t1) => simplifyInvariant(t1)
	    case OR (t1, TRUE()) => TRUE()
	    case OR (TRUE(),t1) => TRUE()

			case OR(OR(t1, t2), t3) => simplifyInvariant(flattenOr(t1, t2, t3))
			case OR(t1, OR(t2, t3)) => simplifyInvariant(flattenOr(t1, t2, t3))

			case OR(BIGOR(Nil), t1) => t1
			case OR(BIGOR(sublist), t1) => simplifyInvariant(flattenBigOr(sublist.head, sublist.tail ++ (t1 :: Nil)))
			case OR(t1, BIGOR(sublist)) => simplifyInvariant(flattenBigOr(t1, sublist))

			case OR(t1,t2) => OR(simplifyInvariant(t1), simplifyInvariant(t2))

			// BIGOR
      case BIGOR(Nil) => FALSE()  // Consider BIGOR to be FALSE v v(list)
			case BIGOR(t1 :: Nil) => simplifyInvariant(t1)
			case BIGOR(t1 :: t2 :: Nil) => simplifyInvariant(OR(t1, t2))
			case BIGOR(sublist) =>
			{
				val simpleList = simplifyOrList(sublist)

				if (simpleList.length > 2) BIGOR(simpleList)
				else if (simpleList.length == 2) OR(simpleList.head, simpleList(1))
        else if (simpleList.length == 0) FALSE()  // Consider BIGOR to be FALSE v v(list)
        else simpleList.head
			}

			// AND
			case AND(t1, TRUE()) => simplifyInvariant(t1)
			case AND(TRUE(),t1) => simplifyInvariant(t1)
	    case AND(FALSE(),t1) => FALSE();
	    case AND(t1, FALSE()) => FALSE();

			case AND(AND(t1, t2), t3) => simplifyInvariant(flattenAnd(t1, t2, t3))
			case AND(t1, AND(t2, t3)) => simplifyInvariant(flattenAnd(t1, t2, t3))

			case AND(BIGAND(Nil), t1) => t1
			case AND(BIGAND(sublist), t1) => simplifyInvariant(flattenBigAnd(sublist.head, sublist.tail ++ (t1 :: Nil)))
			case AND(t1, BIGAND(sublist)) => simplifyInvariant(flattenBigAnd(t1, sublist))

			case AND(t1,t2) => AND(simplifyInvariant(t1), simplifyInvariant(t2));

			// BIGAND
			case BIGAND(Nil) => TRUE()  // Consider a BIGAND to be TRUE ^ ^(list)
			case BIGAND(t1 :: Nil) => simplifyInvariant(t1)
			case BIGAND(t1 :: t2 :: Nil) => simplifyInvariant(AND(t1, t2))
			case BIGAND(sublist) =>
				{
					val simpleList = simplifyAndList(sublist)

					if (simpleList.length > 2) BIGAND(simpleList)
          else if (simpleList.length == 2) AND(simpleList.head, simpleList(1))
          else if (simpleList.length == 0) TRUE() // Consider a BIGAND to be TRUE ^ ^(list)
					else simpleList.head
				}

			// OccupyBox
	   	case OccupyBox(x1,y1,x2,y2) => OccupyBox(x1,y1,x2,y2)
	  	case OccupyPoint(x,y) => OccupyPoint(x,y);

			// NOT
			case NOT(TRUE()) => FALSE()
			case NOT(FALSE()) => TRUE()
			case NOT(NOT(t)) => simplifyInvariant(t)
			case NOT(IMPLIES(premise,conclusion)) => simplifyInvariant( AND(premise, NOT(conclusion)) )
			case NOT(AND(t1,t2)) => simplifyInvariant( OR(NOT(t1), NOT(t2)) )
			case NOT(BIGAND(sublist)) => BIGOR( sublist map NOT)
			case NOT(t) => NOT(simplifyInvariant(t))

			// IMPLIES
			case IMPLIES(FALSE(),t2) => TRUE()
	  	case IMPLIES(TRUE(),t2) => simplifyInvariant(t2)
	  	case IMPLIES(t1,t2) => IMPLIES (simplifyInvariant(t1),simplifyInvariant(t2))

			// TimeStamp
	  	case IntTimeStamp (timestamp) => IntTimeStamp(timestamp);
	    case TimeStamp (timestamp) => TimeStamp(timestamp);

			// Boolean
	  	case TRUE () => TRUE();
	  	case FALSE () => FALSE();

			// Default Case
	  	case i => i;
	  }
	}



	def simplifyAndList[T <: Invariant] (invList: List[T]) : List[T] =
  {
    val flatList = flattenAndList(invList)
    
    val resultList = flatList match
		{
      // Logic reduction
			case Nil                                => Nil //List(TRUE())
			case FALSE()         :: _               => List(FALSE())
      case TRUE()          :: Nil             => List(TRUE())
      case TRUE()          :: tail            => simplifyAndList(tail)
            
      // Recursive application of simplify
			case irreducible     :: tail            =>
			{
				val simplifiedInvariant: Invariant = simplifyInvariant(irreducible)

				if (simplifiedInvariant != irreducible)
          simplifyAndList( simplifiedInvariant :: tail )  // Keep simplifying the head invariant until it does not simplify any further.
				else
        {          
          // Now that the head is not reducible, put it at the tail and reduce the rest.
          val simplifiedTail = simplifyAndList(tail)
          
          simplifiedTail match
          {
            case Nil                  => List(irreducible)
            case FALSE()  :: _        => List(FALSE())
            case TRUE() :: Nil        => List(irreducible)
            case TRUE() :: tail       => irreducible :: tail
            case list                 => irreducible :: list
          }
        }
			}
    }

		resultList.asInstanceOf[List[T]]
	}



	def simplifyOrList[T <: Invariant](invList: List[Invariant]) : List[Invariant] =
  {
    val flatList = flattenOrList(invList)
    
    val resultList = flatList match
		{
      // Logic reduction
			case Nil                               => Nil //List(FALSE())
      case TRUE()         :: _               => List(TRUE())
      case FALSE()        :: Nil             => List(FALSE())
      case FALSE()        :: tail            => simplifyOrList(tail)
            
      // Recursive application of simplify
			case irreducible    :: tail            =>
			{
				val simplifiedInvariant: Invariant = simplifyInvariant(irreducible)

				if (simplifiedInvariant != irreducible)
          simplifyOrList( simplifiedInvariant :: tail )  // Keep simplifying the head invariant until it does not simplify any further.
				else
        {
          // Now that the head is not reducible, put it at the tail and reduce the rest.
          val simplifiedTail = simplifyOrList(tail)
          
          simplifiedTail match
          {
            case Nil                  => List(irreducible)
            case TRUE()  :: _         => List(TRUE())
            case FALSE() :: Nil       => List(irreducible)
            case FALSE() :: tail      => irreducible :: tail
            case list                 => irreducible :: list
          }
        }
			}
		}

		resultList.asInstanceOf[List[T]]
	}



	// ----------------------------------------------------------- Aggregation

	type Aggregate[T] = (T, T) => T



	// ----------------------------------------------------------- Behavioural Maps
	
	class BeMap[P <: Invariant, C <: Invariant](terms: List[IMPLIES[P,C]]) extends BIGAND[IMPLIES[P,C]](terms)
	{
		type PremiseSet = Set[P]
		type ConclusionSet = Set[C]
		type I = IMPLIES[P,C]

		lazy val premises:    PremiseSet = ( this.terms map { _.premise } ).toSet[P]
		lazy val conclusions: ConclusionSet = ( this.terms map { _.conclusion } ).toSet[C]
		lazy val elements:    Set[Any] = premises.toSet[Any] union conclusions.toSet[Any]

		def apply(key: Invariant): Option[C] =
		{
			val optionalImplication: Option[I] = this.terms find { i: I => i.premise == key }

			val result = optionalImplication match
			{
				case None                 => None
				case Some(implication: I) => Some(implication.conclusion)
			}

			result
		}
	}

	object BeMap  // Companion Object
	{
		//def apply[T <: Invariant](): BeMap[T]                                  = BeMap[T](Nil)
		//def apply[T <: Invariant](terms: Set[IMPLIES[Invariant,T]]):  BeMap[T] = BeMap(terms.toList)
		def apply[P <: Invariant, C <: Invariant](terms: List[IMPLIES[P,C]] = Nil): BeMap[P,C] = new BeMap(terms)
		def apply[P <: Invariant, C <: Invariant](bigand: BIGAND[IMPLIES[P,C]]): BeMap[P,C] = new BeMap(bigand.terms)
		//def apply[P <: Invariant, C <: Invariant](and: AND): BeMap[P,C] = new BeMap(List(and.t1, and.t2))
	}


	// Operations

	def unionMapList[P <: Invariant, C <: Invariant](mapList: List[BeMap[P,C]], f: Aggregate[C]): BeMap[P,C] =
	{
		mapList match
		{
			case Nil => new BeMap[P,C](Nil)
			//case Nil => BeMap[C]
			case bm :: Nil => bm
			case bm1 :: bm2 :: Nil => unionMaps(bm1, bm2, f)
			case bm1 :: bm2 :: tail => unionMapList( unionMaps(bm1, bm2, f) :: tail, f )
		}
	}

	def unionMaps[P <: Invariant, C <: Invariant](mapA: BeMap[P,C], mapB: BeMap[P,C], f: Aggregate[C]): BeMap[P,C] =
	{
		type PremiseSet = Set[P]

		val premiseSetA     : PremiseSet = mapA.premises
		val premiseSetB     : PremiseSet = mapB.premises
		val premiseSetAB    : PremiseSet = premiseSetA intersect premiseSetB
		val premiseSetAOnly : PremiseSet = premiseSetA diff premiseSetB
		val premiseSetBOnly : PremiseSet = premiseSetB diff premiseSetA

		type I = IMPLIES[P, C]

		val pairsAOnly  : Set[I] = premiseSetAOnly map { p: P => IMPLIES[P,C](p, mapA(p).get) }
		val pairsBOnly  : Set[I] = premiseSetBOnly map { p: P => IMPLIES[P,C](p, mapB(p).get) }
		val pairsAB     : Set[I] = premiseSetAB    map { p: P => IMPLIES[P,C](p, f(mapA(p).get, mapB(p).get)) }

		val allPairs = pairsAOnly | pairsAB | pairsBOnly

		BeMap( allPairs.toList )
	}


	

	// --------------------------------------------------------------- Collision Detection

	def collisionTestsAlt1(subinvs1 : List[Invariant], subinvs2 : List[Invariant]) : Boolean ={
	  val collect : HashSet[Int] = new HashSet[Int]();
    
	  // try {
		  for (subinv1 <- subinvs1) {
			  for (e <- encodeAsSat(subinv1,Nil)) {
			   collect.add(e);
			  }
		  }
		  for (subinv2 <- subinvs2) {
			  for (e <- encodeAsSat(subinv2,Nil)) {
			    if (collect.apply(e)) return true;
			  }
		  }
      return (false);
	}
	
	// for larger numbers, esp. 3D applications
	def collisionTestsBig(subinvs1 : List[Invariant], subinvs2 : List[Invariant]) : Boolean ={
	  val collect : HashSet[BigInt] = new HashSet[BigInt]();
    
	  // try {
		  for (subinv1 <- subinvs1) {
			  for (e <- encodeAsSatLarge(subinv1,Nil)) {
			   collect.add(e);
			  }
		  }
		  for (subinv2 <- subinvs2) {
			  for (e <- encodeAsSatLarge(subinv2,Nil)) {
			    if (collect.apply(e)) return true;
			  }
		  }
      return (false);
	}

	// this is the inclusion test,
	// for larger numbers, esp. 3D applications
	//corrected on 10/10/2014
	def inclusionTestsBig(subinvs1 : List[Invariant], subinvs2 : List[Invariant]) : Boolean ={
	  val collect : HashSet[BigInt] = new HashSet[BigInt]();
    
	  // try {
		  for (subinv2 <- subinvs2) {
			  for (e <- encodeAsSatLarge(subinv2,Nil)) {
			   collect.add(e);

			  }
		  }
		  for (subinv1 <- subinvs1) {
			  for (e <- encodeAsSatLarge(subinv1,Nil)) {
			    if (!collect.contains(e)) return false;

			  }
		  }
      return (true);
	}
	 
	// works best for TimeStamp not IntTimeStamp
  // TimePoint added 21/04/2015, not yet tested
  
	def getSubInvariantForTime[T](timestamp : T,inv : Invariant) : Invariant ={
	  inv match {
	    case (BIGAND ( (IMPLIES (TimeStamp (t),subinv1))::l)) =>  if (t.equals(timestamp)) subinv1 else getSubInvariantForTime (timestamp, BIGAND(l));
	    case (BIGAND ( (IMPLIES (AND(TimeStamp (t),Event(e)),subinv1))::l)) =>  if (t.equals(timestamp)) (AND(IMPLIES(Event(e),subinv1), getSubInvariantForTime(timestamp,BIGAND (l)))) else getSubInvariantForTime (timestamp, BIGAND(l));
	   	case (BIGAND ( (IMPLIES (AND(TimeStamp (t),NOT(Event(e))),subinv1))::l)) =>  if (t.equals(timestamp)) (AND(IMPLIES(NOT(Event(e)),subinv1), getSubInvariantForTime(timestamp,BIGAND (l)))) else getSubInvariantForTime (timestamp, BIGAND(l));
	    case (IMPLIES (IntTimeStamp (i), subinv1)) => if (i == timestamp) subinv1 else TRUE();
	  	case (IMPLIES (TimeStamp (t), subinv1)) => if (t.equals(timestamp)) subinv1 else TRUE();
	  	case (IMPLIES (AND (TimeStamp (t),Event(e)), subinv1)) => if (t.equals(timestamp)) (IMPLIES (Event(e),subinv1)) else TRUE();
	  	case (IMPLIES (AND (TimeStamp (t),NOT (Event(e))), subinv1)) => if (t.equals(timestamp)) (IMPLIES (NOT (Event(e)),subinv1)) else TRUE();
	  	case (AND (IMPLIES (IntTimeStamp (i),subinv1), subinv2)) => if (i == timestamp) subinv1 else getSubInvariantForTime (timestamp, subinv2);
	  	case (AND (IMPLIES (TimeStamp (t),subinv1), subinv2)) => if (t.equals(timestamp)) subinv1 else getSubInvariantForTime (timestamp, subinv2);
	    case (AND (IMPLIES (AND (TimeStamp (t),Event (e)),subinv1), subinv2)) => if (t.equals(timestamp)) AND(IMPLIES(Event(e),subinv1),getSubInvariantForTime (timestamp, subinv2)) else getSubInvariantForTime (timestamp, subinv2);
	    case (AND (IMPLIES (AND (TimeStamp (t),NOT (Event (e))),subinv1), subinv2)) => if (t.equals(timestamp)) AND(IMPLIES(NOT (Event(e)),subinv1),getSubInvariantForTime (timestamp, subinv2)) else getSubInvariantForTime (timestamp, subinv2);
	    case (AND (subinv2, IMPLIES (IntTimeStamp (i),subinv1))) => if (i == timestamp) subinv1 else getSubInvariantForTime (timestamp, subinv2);
	  	case (AND (subinv2, IMPLIES (TimeStamp (t),subinv1))) => if (t.equals(timestamp)) subinv1 else getSubInvariantForTime (timestamp, subinv2);
	  	case (AND (subinv2, IMPLIES (AND(TimeStamp (t),Event(e)),subinv1))) => if (t.equals(timestamp)) AND(IMPLIES(Event(e),subinv1),getSubInvariantForTime(timestamp,subinv2)) else getSubInvariantForTime (timestamp, subinv2);
		  case (AND (subinv2, IMPLIES (AND(TimeStamp (t),NOT (Event(e))),subinv1))) => if (t.equals(timestamp)) AND(IMPLIES(NOT (Event(e)),subinv1), getSubInvariantForTime(timestamp,subinv2)) else getSubInvariantForTime (timestamp, subinv2);
    
    //TimePoint block added 21/04/2015
      case (BIGAND ( (IMPLIES (TimePoint (t),subinv1))::l)) =>  if (t.equals(timestamp)) subinv1 else getSubInvariantForTime (timestamp, BIGAND(l));
      case (BIGAND ( (IMPLIES (AND(TimePoint (t),Event(e)),subinv1))::l)) =>  if (t.equals(timestamp)) (AND(IMPLIES(Event(e),subinv1), getSubInvariantForTime(timestamp,BIGAND (l)))) else getSubInvariantForTime (timestamp, BIGAND(l));
      case (BIGAND ( (IMPLIES (AND(TimePoint (t),NOT(Event(e))),subinv1))::l)) =>  if (t.equals(timestamp)) (AND(IMPLIES(NOT(Event(e)),subinv1), getSubInvariantForTime(timestamp,BIGAND (l)))) else getSubInvariantForTime (timestamp, BIGAND(l));
      case (IMPLIES (TimePoint (t), subinv1)) => if (t.equals(timestamp)) subinv1 else TRUE();
      case (IMPLIES (AND (TimePoint (t),Event(e)), subinv1)) => if (t.equals(timestamp)) (IMPLIES (Event(e),subinv1)) else TRUE();
      case (IMPLIES (AND (TimePoint (t),NOT (Event(e))), subinv1)) => if (t.equals(timestamp)) (IMPLIES (NOT (Event(e)),subinv1)) else TRUE();
      case (AND (IMPLIES (TimePoint (t),subinv1), subinv2)) => if (t.equals(timestamp)) subinv1 else getSubInvariantForTime (timestamp, subinv2);
      case (AND (IMPLIES (AND (TimePoint (t),Event (e)),subinv1), subinv2)) => if (t.equals(timestamp)) AND(IMPLIES(Event(e),subinv1),getSubInvariantForTime (timestamp, subinv2)) else getSubInvariantForTime (timestamp, subinv2);
      case (AND (IMPLIES (AND (TimePoint (t),NOT (Event (e))),subinv1), subinv2)) => if (t.equals(timestamp)) AND(IMPLIES(NOT (Event(e)),subinv1),getSubInvariantForTime (timestamp, subinv2)) else getSubInvariantForTime (timestamp, subinv2);
      case (AND (subinv2, IMPLIES (TimePoint (t),subinv1))) => if (t.equals(timestamp)) subinv1 else getSubInvariantForTime (timestamp, subinv2);
      case (AND (subinv2, IMPLIES (AND(TimePoint (t),Event(e)),subinv1))) => if (t.equals(timestamp)) AND(IMPLIES(Event(e),subinv1),getSubInvariantForTime(timestamp,subinv2)) else getSubInvariantForTime (timestamp, subinv2);
      case (AND (subinv2, IMPLIES (AND(TimePoint (t),NOT (Event(e))),subinv1))) => if (t.equals(timestamp)) AND(IMPLIES(NOT (Event(e)),subinv1), getSubInvariantForTime(timestamp,subinv2)) else getSubInvariantForTime (timestamp, subinv2);
    
	  	case i => TRUE();
	  }
	}
  
  //added 22/04/2015
  //maybe to specific, move to other location?
  //not really tested yet
  
  /*AndOverapproximateTimeNodes*/
  def fillTimePoints  (starttime : Long, endtime : Long, inv : Invariant) : Invariant ={
    var retlist :List[Invariant]= Nil
    for ( i :Long <- starttime to endtime) {
        retlist ::= getSubInvariantForTime(i,inv)
    }
    return BIGAND(retlist)
  } 
	
	def ownInvariant[C](c: C,inv:Invariant) : Invariant ={
	  inv match {
	   	case BIGOR (l) => BIGAND (l.map (x => ownInvariant (c,x))); 
	    case BIGAND (l) => BIGAND (l.map (x => ownInvariant (c,x))); 
	  	case OccupyBox(x1,y1,x2,y2) => OwnBox(c,x1,y1,x2,y2)
	    case OwnBox(c2,x1,y1,x2,y2) =>  OwnBox(c,x1,y1,x2,y2) //new owner set
	  	case OccupyPoint(x,y) => OwnPoint(c,x,y); 
	  	case OwnPoint(c2,x,y) => OwnPoint(c,x,y); //new owner set
	  	case OR (t1,t2) => OR (ownInvariant(c,t1),ownInvariant(c,t2));
	  	case AND (t1,t2) => AND (ownInvariant(c,t1),ownInvariant(c,t2));
	  	case NOT (t) => NOT (ownInvariant(c,t));
	  	case IMPLIES(t1,t2) => IMPLIES (ownInvariant(c,t1),ownInvariant(c,t2)); 
	  	case IntTimeStamp (timestamp) => IntTimeStamp(timestamp);
	  	case TimeStamp (timestamp) => TimeStamp(timestamp);
	  	case OccupyNode (n) => OwnNode(c,n)
	  	case OwnNode(c2,n) => OwnNode(c,n) // new owner set
	  	case TRUE () => TRUE ();
	  	case FALSE () => FALSE ();
	  	case inv => inv;
	  }
	  
	}
	
	def unownInvariant[C](inv:Invariant) : Invariant ={
	  inv match {
	    
	   	case BIGOR (l) => BIGAND (l.map (x => unownInvariant (x))); 
	    case BIGAND (l) => BIGAND (l.map (x => unownInvariant (x))); 
	  	case OccupyBox(x1,y1,x2,y2) => OccupyBox(x1,y1,x2,y2)
	    case OwnBox(c2,x1,y1,x2,y2) =>  OccupyBox(x1,y1,x2,y2)
	  	case OccupyPoint(x,y) => OccupyPoint(x,y)
	  	case OwnPoint(c2,x,y) => OccupyPoint(x,y)
	  	case OR (t1,t2) => OR (unownInvariant(t1),unownInvariant(t2));
	  	case AND (t1,t2) => AND (unownInvariant(t1),unownInvariant(t2));
	  	case NOT (t) => NOT (unownInvariant(t));
	  	case IMPLIES(t1,t2) => IMPLIES (unownInvariant(t1),unownInvariant(t2)); 
	  	case IntTimeStamp (timestamp) => IntTimeStamp(timestamp);
	  	case TimeStamp (timestamp) => TimeStamp(timestamp);
	  	case OccupyNode (n) => OccupyNode(n)
	  	case OwnNode(c2,n) => OccupyNode(n)
	  	case TRUE () => TRUE ();
	  	case FALSE () => FALSE ();
	  	case inv => inv;
	  }
	  
	}
	
	// Must obey a very specific format at the moment
	def projectEventCondition[E] (event : E,inv : Invariant) : Invariant ={
	  inv match {
	    case (BIGAND (Nil)) => FALSE();
	    case (BIGAND ((IMPLIES (event2,inv2) )::l)) => if (event.equals(event2)) inv2 else projectEventCondition(event,BIGAND (l));
      	case default => FALSE();
	  }  
	}

	// Major changed version of projectEventCondition 
	// slightly different (corrected?) for BIGAND case
	// needs finishing 19/06/2014, yes, but seems to work in tests as of 23/06/2014
	// fixed on 13/10/2014
	def projectCondition[C] (condition : C,inv : Invariant) : Invariant ={
	  inv match {
	    case (IMPLIES (condition2,inv2)) => if (condition2.equals(condition)) inv2 else FALSE()
	    //case (BIGAND (Nil)) => FALSE();
	    case (BIGAND (l)) => BIGAND(l.map (x =>  projectCondition(condition,x)))
      	case default => default;
	  }  
	}



	// ----------------------------------------------------- Boolean Satisfiability Solving (SAT)

 /*
  * Encode as SAT section
  * note, some of the changes made on 24/04/2015 are untested
  */
	
	def encodeAsSat (inv : Invariant,list : List[Int]) : List[Int] ={
    
	  inv match {
	    case (BIGAND (Nil)) => Nil;
	    case (BIGAND (x::l)) => encodeAsSat(x,Nil) ++ encodeAsSat(BIGAND(l),Nil);
      	case (AND (inv1,inv2)) => encodeAsSat(inv1,Nil) ++ encodeAsSat(inv2,Nil); 
      	case (OccupyPoint (x,y)) => (x+y*100)::Nil;
        
        //for Nodes, addedd 22/04/2015
        case (OccupyNode(n)) =>  n match {case (n : Int) => (n::Nil) case _ => Nil};
        
      	case default => Nil
	  }
	}
	
	//using large integers for larger encodings
	//test this in the future 22/01/2014, should be correct
  //added stuff on 22/04/2015, needs testing
	def encodeAsSatLarge (inv : Invariant,list : List[BigInt]) : List[BigInt] ={
    
	  inv match {
	    case (BIGAND (Nil)) => Nil;
	    case (BIGAND (x::l)) => encodeAsSatLarge(x,Nil) ++ encodeAsSatLarge(BIGAND(l),Nil);
      	case (AND (inv1,inv2)) => encodeAsSatLarge(inv1,Nil) ++ encodeAsSatLarge(inv2,Nil); 
      	case (OccupyPoint (x,y)) => (x+(y*BigInt.apply(1000)))::Nil;
      	case (Occupy3DPoint (x,y,z)) => (x+(y*BigInt.apply(1000))+(z*BigInt.apply(1000000)))::Nil;
        
        //for Nodes, addedd 22/04/2015
        case (OccupyNode(n)) =>  n match {
                
            case (n : Long) => (n::Nil) 
            case (n : Int) => (n::Nil)            
            case (n : BigInt) => (n::Nil)
            case (n : String) => (n.toLong::Nil)
            case _ => Nil};
          
       

        
      	case default => Nil
	  }
	}
	
	//untested 
	def encodeAsSatVeryLarge (inv : Invariant,list : List[BigInt]) : List[BigInt] ={
    
	  inv match {
	    case (BIGAND (Nil)) => Nil;
	    case (BIGAND (x::l)) => encodeAsSatLarge(x,Nil) ++ encodeAsSatLarge(BIGAND(l),Nil);
      	case (AND (inv1,inv2)) => encodeAsSatLarge(inv1,Nil) ++ encodeAsSatLarge(inv2,Nil); 
      	case (OccupyPoint (x,y)) => (x+(y*BigInt.apply(10000)))::Nil;
      	case (Occupy3DPoint (x,y,z)) => (x+(y*BigInt.apply(10000))+(z*BigInt.apply(100000000)))::Nil;
        
        //for Nodes, addedd 22/04/2015
        case (OccupyNode(n)) =>  n match {
                
            case (n : Long) => (n::Nil) 
            case (n : Int) => (n::Nil)            
            case (n : BigInt) => (n::Nil)
            case (n : String) => (n.toLong::Nil)
            case _ => Nil};
      	case default => Nil
	  }
	}
  
  /*
   * End: EncodeAsSatSection
   */
	
	//check types for collisions
	//not finished yet 31/01/2014
	// missing:
	// - attachments and attachmentpoints
	// - testing (two tests passed: 03/02/2014)
	// - box abstractions yes or no; yes: 03/02/2014
	// - unfolding added : 03/02/2014

	def checkTypeCollisionCompatible (bt1 : SpatioTemporalBehavioralType, bt2 : SpatioTemporalBehavioralType, mintime : Int, maxtime : Int) : Boolean ={
	  (bt1,bt2) match {
	    case (SimpleSpatioTemporalBehavioralType (id, behavior, attachmentpoints,attachmentpointsbehavior),SimpleSpatioTemporalBehavioralType (id2 , behavior2 , attachmentpoints2 ,attachmentpointsbehavior2 )) =>
	      var checkresult : Boolean = true
	    		  for (i <- mintime to maxtime) {
	    			  checkresult &= ! collisionTestsBig (unfoldInvariant(OccupySegment2OccupyBoxes3D(this.getSubInvariantForTime(i,behavior)))::Nil,
	    			       unfoldInvariant(OccupySegment2OccupyBoxes3D(this.getSubInvariantForTime(i,behavior2))):: Nil)
	    			  //println(checkresult)
	    		  }
	      
	     return checkresult
	    case default => false
	  }

	  
	  return false
	}
	
	
	//add points to a hash set
	// added 06/06/2014
	//untested
	// of course it works only for OccupyPoints at top level at this stage
		
	
	def extractPoints (inv : Invariant) : HashSet[(Int,Int)] ={
	  var hs = new HashSet[(Int,Int)]()	 	  
	  return extractPointsH(inv,hs);
	}

	def extractPointsH (inv : Invariant, hs : HashSet[(Int,Int)]) : HashSet[(Int,Int)] ={
		inv match {
		  		case BIGAND (OccupyPoint(xp,yp)::xs) => extractPointsH (BIGAND(xs),hs+=((xp,yp)))
		  		case BIGAND (x::xs) => extractPointsH (BIGAND(xs),hs)
		  		case _ => hs
		}
	}
	
	//counts occupied points in the (minX,minY,maxX,maxY) plane 
	// added 06/06/2014
	//untested
	
	
	def countPoints (inv:Invariant,minX : Int, minY: Int,maxX : Int, maxY :Int) : Int ={
	  var counter : Int = 0
	  var hs = extractPoints(inv)
	  
	  
	  for (x <- minX to maxX) {
	    for (y <- minY to maxY) {
	      if (hs.apply(x,y)) counter += 1
	    }
	  }
	  return counter
	}

	//same, but without min max values
	//untested
	def countPoints (inv:Invariant) : Int ={
	  var hs = extractPoints(inv)
	  

	  return hs.size
	}

	/* Some geometric projection selection
	 * started adding 25/06/2014
	 */
	//selects objects within radius r of a point (x,y)
	//only works for "owner -> point" so far
	//tested as of 25/06/2014 seems ok
	def selectObjectWithinRadius (inv : Invariant,x : Int,y : Int,r : Int) : Invariant ={
	  inv match {
	    case BIGAND(l) => BIGAND(l.map(i1 => selectObjectWithinRadius(i1,x,y,r)))
	    case IMPLIES(c,OccupyPoint(x1,y1)) => if (math.abs(x - x1) * math.abs (x - x1) + math.abs(y - y1) * math.abs(y - y1) <= r * r) IMPLIES(c,OccupyPoint(x1,y1)) else FALSE()
	    case _ => FALSE() 
	  }
	}

}

 
// ----------------------------------------------------------- Behavioural Graphs
	
	//case class BeGraph2[N](terms: List[Edge[N]])
	

  // Annotated Version

	class BeGraphAnnotated[+N, +A](terms: List[EdgeAnnotated[N, A]]) extends BIGAND[EdgeAnnotated[N, A]](terms)
	{
		lazy val sources: List[N] = (terms map { _.source }) 
		lazy val targets: List[N] = (terms map { _.target })
		lazy val nodes:   List[N] = sources union targets

		/**
	   * Finds all the destination nodes connected to a given source node.
	   * 
	   * @typeParam N is the type of the nodes.
	   * 
	   * @param source is the source node.
	   * @returns a list of the destination nodes.
		 */
		def apply(source: Any): List[N] =
		{
			val optionalEdge: List[EdgeAnnotated[N, A]] = this.terms filter { _.source == source }

			val result = optionalEdge match
			{
				case Nil  => Nil
				case list => list map { _.target }
			}

			result
		}
		
	}
	
	object BeGraphAnnotated  // Companion Object
	{
    val core = standardDefinitions; import core._
    
		//def apply[T <: Invariant](): BeMap[T]                                  = BeMap[T](Nil)
		//def apply[T <: Invariant](terms: Set[IMPLIES[Invariant,T]]):  BeMap[T] = BeMap(terms.toList)
		def apply[N,A](terms: List[EdgeAnnotated[N,A]] = Nil) = new BeGraphAnnotated(terms)
		def apply[N,A](bigand: BIGAND[EdgeAnnotated[N,A]]) = new BeGraphAnnotated(bigand.terms)
		//def apply[N](and: AND): BeGraph[N] = new BeGraph(List(and.t1, and.t2))
	}

	
	// Un-annotated version
	
	class BeGraph[+N](terms: List[Edge[N]]) extends BeGraphAnnotated[N, Nothing](terms)

	object BeGraph  // Companion Object
	{    
		def apply[N,A](terms: List[Edge[N]] = Nil) = new BeGraph(terms)
		def apply[N,A](bigand: BIGAND[Edge[N]]) = new BeGraph(bigand.terms)
	}





