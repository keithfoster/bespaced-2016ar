import org.scalatest._

import scala.util.Either

/**
 * @author Keith Foster
 */



package object BeSpaceDCore {


  //-------------------------------- Standard Package includes

  import HazyTypes._



  //-------------------------------- Global Definitions
  private
  val CoreDefinitions = new CoreDefinitions()

  private
  val GraphOperations = new GraphOperations()



  //-------------------------------- Inversion of Control

  //TODO: Add some factory functions for core definitions here
  //      A factory function should be passed as a parameter to Apps and test suites
  //      so they can instantiate a core definition object to use.

  def standardDefinitions = CoreDefinitions
  val core  = standardDefinitions; import core._
  
  
  
  //-------------------------------- JSON Serialisation
  
    trait Jsonable
    {
      def toJson: String
    }
  
  
    def edgeToJson[N, A](edge: EdgeAnnotated[N, A]): String =
		{
      val annotate: Boolean        = edge.annotation.isDefined
      val annotationString: String = if (annotate) 
        s"""
                        "annotation" : ${toJson(edge.annotation.get)}
        """ 
      else 
        ""
        
		  return s"""
		      {
		              "type" : "EdgeAnnotated"
		            "source" : ${toJson(edge.source)}
		            "target" : ${toJson(edge.target)}"${annotationString}
		      }
		    """
		}

    def graphToJson[N, A](graph: BeGraphAnnotated[N, A]): String =
		{
		  import scala.compat.Platform.EOL
		  
		  val edges: List[String] = (graph.terms map {e: EdgeAnnotated[N,A] => edgeToJson(e)})
		  
		  return s"""
		    {
		      "type" : "BeGraphAnnotated"
		      "edges" : [
		        ${edges mkString s"$EOL      "}
		]
		    }
		    """
		}

  
  
  // ------------------------------- Implicits
  
  // Time Orderings
  implicit val intTimeOrdering = { _: Int => Ordering[Int] } 

  
  
  // ------------------------------- Implicit Operators

  implicit class InvariantOps[N <: Invariant, L <: N](private val left: L) extends AnyVal
  {
    // IMPLIES Construction
    @inline def implies[R <: Invariant] (right : R) = IMPLIES[L,R](left, right)
    @inline def ==>[R <: Invariant] (right : R) = implies(right)
    @inline def →[R <: Invariant] (right : R) = ==>(right)
    
    // AND
    @inline def and[R <: Invariant] (right : R) = AND(left, right)
    @inline def ^[R <: Invariant] (right : R) = and(right)

    // OR
    @inline def or[R <: Invariant] (right : R) = OR(left, right)
    @inline def v[R <: Invariant] (right : R) = or(right)
    
    // OR
    @inline def xor[R <: Invariant] (right : R) = XOR(left, right)
    @inline def +[R <: Invariant] (right : R) = or(right)
    
    // Edges
    @inline def edge[R <: N] (right : R) = Edge[N](left, right)
    @inline def -->[R <: N] (right : R) = edge(right)
        

    // MORE TODO...
  }

  implicit class AndOps[L <: AND](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGAND(List(left.t1, left.t2, right))).asInstanceOf[BIGAND[R]]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(OR(left, right)).asInstanceOf[OR]
  }

  implicit class BeGraphOps[N, A, E <: EdgeAnnotated[N,A], LN <: N, L <: BeGraph[LN]](private val left: L) extends AnyVal
  {
    // JOIN
    @inline def ++[RN <: N, R <: BeGraphAnnotated[RN,A]] (right : R) = BeGraphAnnotated[N,A](standardDefinitions.flattenAndList(left.terms ::: right.terms).asInstanceOf[List[E]])
  }


  implicit class BigandOps[L <: BIGAND[Invariant]](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : BIGAND[R]) = standardDefinitions.flatten(BIGAND(left.terms ::: right.terms)).asInstanceOf[BIGAND[R]]
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGAND(left.terms ::: List(right))).asInstanceOf[BIGAND[R]]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(OR(left, right)).asInstanceOf[OR]
  }

  implicit class OrOps[L <: OR](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(AND(left, right)).asInstanceOf[AND]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGOR(List(left.t1, left.t2, right))).asInstanceOf[BIGOR[R]]
  }

  implicit class BigorOps[L <: BIGOR[Invariant]](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(AND(left, right)).asInstanceOf[AND]

    // OR
    @inline def v[R <: Invariant] (right : BIGOR[R]) = standardDefinitions.flatten(BIGOR(left.terms ::: right.terms)).asInstanceOf[BIGOR[R]]
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGOR(left.terms ::: List(right))).asInstanceOf[BIGOR[R]]
  }

  
  implicit class XorOps[L <: XOR[Invariant]](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(AND(left, right)).asInstanceOf[AND]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(OR(left, right)).asInstanceOf[OR]
    
    // XOR
    @inline def +[R <: Invariant] (right : R) = standardDefinitions.flatten(XOR(left.terms ::: List(right))).asInstanceOf[XOR[R]]
  }

  
  // ------------------------------- Obsolete

  // Flatten and Unwrap the terms of conjunctions

  @Deprecated
  def flattenAnds(presumedAnd: Invariant): Invariant = CoreDefinitions.simplifyInvariant(BIGAND(unwrapAnds(presumedAnd)))

  @Deprecated
  def unwrapAnds(presumedAnd: Invariant): List[Invariant] =
  {
    presumedAnd match
    {
      case AND(t1, t2)                   => unwrapAnds(t1) ++ unwrapAnds(t2)
      case BIGAND(list: List[Invariant]) => list flatMap unwrapAnds
      case _                             => List(presumedAnd)
    }
  }



  //-------------------------------- Scala Language Improvements

  // Union Types
  type or[L, R] = Either[L, R] // Move this to a common module
  implicit def l2Or[L, R](l: L): L or R = Left(l)
  implicit def r2Or[L, R](r: R): L or R = Right(r)



  //-------------------------------- ScalaTest Base classes
  
  // Unit Testing
  abstract class UnitSpec extends FlatSpec with Matchers with OptionValues with Inside with Inspectors

}