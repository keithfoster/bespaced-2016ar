package amqprunner;

import amqp.*;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class amqprunner {
	
	private final static String QUEUE_NAME = "hello";

	public static void main(String[] args) throws Exception{
		while(true){
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("118.138.241.187");
		factory.setUsername("test");
		factory.setPassword("password");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		String message = AMQPproducer.getString();
		
			channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
			channel.close();
			connection.close();
			Thread.sleep(1000);
		}
		
	}

}
