package BeSpaceDExamples

import BeSpaceDCore._

object GraphTest3 extends GraphOperations{

  def examplegraph1 = BIGAND(Nil)

  def examplegraph2 = BIGAND(
      Edge(1,2)::
      Edge(2,3)::
      Nil)

  def examplegraph3 = BIGAND(
      Edge(1,2)::
      Edge(2,3)::
      Edge(3,4)::
      Edge(4,5)::
      Edge(5,6)::
      Edge(4,7)::
      Nil)
      
  def examplegraph3a = BIGAND(
      Edge(1,2)::
      Edge(2,3)::
      Edge(3,4)::
      Edge(4,5)::
      Edge(5,6)::
      Edge(4,7)::
      Edge(6,1)::
      Nil) 
      
  def edgelist (start : Int, end : Int) : List[Invariant] ={
    var retlist : List[Invariant] = Nil
    for (i <- start to (end-1)) {
      retlist ::= Edge(i,i+1)
    }
    return retlist
  }    
      
  def examplegraph4 = BIGAND(
        edgelist(10,20) ++ (Edge(20,10)::Nil)
      )
      
  def examplegraph5 = BIGAND(
    edgelist(1000,1050) ++
    edgelist(1300,1350) ++
    Nil
  )
  
  def main(args: Array[String]) {
    println(this.transitiveHull(invariant2Tuplelist(examplegraph1)).contains((1,1)))
    println("-------")
    println (invariant2Tuplelist(examplegraph2))
    println(this.transitiveHull(invariant2Tuplelist(examplegraph2)))
    println(this.transitiveHull(invariant2Tuplelist(examplegraph2)).contains((1,3)))
    println("-------")
    println (invariant2Tuplelist(examplegraph3))
    println(this.transitiveHull(invariant2Tuplelist(examplegraph3)))
    println("-------")
    println (invariant2Tuplelist(examplegraph3a))
    println(this.transitiveHull(invariant2Tuplelist(examplegraph3a)))
    println("-------")
    println(invariant2Tuplelist(examplegraph4))
    println("-------")
    println(invariant2Tuplelist(examplegraph5))
    println(this.transitiveHull( invariant2Tuplelist(examplegraph5)).size)
    println (this.transitiveHull( invariant2Tuplelist(examplegraph5)).contains(1000,1030))
  }
  
}