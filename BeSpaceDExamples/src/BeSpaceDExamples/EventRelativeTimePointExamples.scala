
/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/

/* needs runtime setting -Xss515m or similar, Sat4j has problems with number of variables, encodeAsSat uses to low range for encoding of x,y variables */

package BeSpaceDExamples
import BeSpaceDCore._;
import BeSpaceDCore.SpatioTemporal._


object EventRelativeTimePointExample extends CoreDefinitions{

  
  val exampleinv1 : Invariant = 
  BIGAND (
      IMPLIES (Event("Go"),BIGAND(           
          EROccupyBox(
              (x : ERTP) => x match {case IntERTP("Go",i : Int) => 10+i} ,
              (y : ERTP) => 10, 
              (x : ERTP) => x match {case IntERTP("Go",i : Int) => 20+i} , 
              (y : ERTP) => 20)
          ::Nil
          )) :: 
      IMPLIES (Event("Break"),BIGAND( 
          IMPLIES(TimeStamp(IntERTP("Break",0)),EROccupyBox((x :ERTP) => x match {case IntERTP("Go",i : Int) => 10+i} ,(y : ERTP) => 0, (x : ERTP) => 0, (y : ERTP) => 0))
          ::Nil
          ))
     ::Nil
  );
  
   val exampleinv2 : Invariant = 
  BIGAND (
      IMPLIES (Event("Go"),BIGAND(           
          OccupyBoxSI(SI_Add( SI_C(10), SI_Var("Go")),SI_C(20),SI_C(10),SI_Add( SI_C(20), SI_Var("Go")))
          ::Nil
          )) :: 
      IMPLIES (Event("Break"),BIGAND( 
          IMPLIES(TimeStamp(IntERTP("Break",0)),EROccupyBox((x :ERTP) => x match {case IntERTP("Go",i : Int) => 10+i} ,(y : ERTP) => 0, (x : ERTP) => 0, (y : ERTP) => 0))
          ::Nil
          ))
     ::Nil
  );
  
  
  def main(args: Array[String]) {
    System.out.println(exampleinv1);
  }
}