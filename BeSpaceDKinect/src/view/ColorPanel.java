/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * DEVELOPED BY Alex Yeap and Vasileios Dimitrakopoulos 2016
 */

package view;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;

import model.FrameProcessor;

/**
 * Observer Design Pattern.
 */
@SuppressWarnings("serial")
public class ColorPanel extends AbstractDisplayPanel implements Observer {
    
    FrameProcessor processor;
    
    public ColorPanel() {
        super();
        setBackground(Color.WHITE);
    }
    
    /**
     * Renders an image on the window from an array of color data.
     * @param color_data The color data.
     * @param width The width of the image.
     * @param height The height of the image.
     */
    public void renderImage(int[] color_data, int width, int height) {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        
        for (int y = 0, counter = 0; y < height; y++) {
            for (int x = 0; x < width; x++, counter++) {
                img.setRGB(x, y, color_data[counter]);
            }
        }
        
        drawer = getGraphics();
        drawer.drawImage(img, 0, 0, width, height, Color.WHITE, null);
        drawer.dispose();
    }
    
    /**
     * Updates the window's content.
     */
    @Override
    public void update(Observable o, Object arg) {
        processor = FrameProcessor.getInstance();

        removeAll();
        renderImage(processor.getColorAsRGBInts(), App.COLOUR_WIDTH, App.COLOUR_HEIGHT);
    }
}