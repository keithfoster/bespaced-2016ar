/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * DEVELOPED BY Alex Yeap and Vasileios Dimitrakopoulos 2016
 */

package view;

import javax.swing.*;
import java.awt.*;


@SuppressWarnings("serial")
public class AbstractDisplayPanel extends JPanel {
    
    protected Graphics drawer;
    
    public AbstractDisplayPanel() {
        setBackground(Color.WHITE);
    }
    
    
    /**
     * Draws a bounding box on the window around an object of interest.
     * @param startX The starting point of the object on the X-axis.
     * @param startY The starting point of the object on the Y-axis.
     * @param endX The ending point of the object on the X-axis.
     * @param endY The ending point of the object on the Y-axis.
     * @param thickness The thickness of the bounding box.
     * @param color The color of the bounding box.
     */
    public void drawBoundingBox(int startX, int startY, int endX, int endY, int thickness, Color color) {
        drawer = getGraphics();
        
        if (color == null) color = Color.RED;
        drawer.setColor(color);
        
        for (int i = 0; i < thickness; ++i) {
            drawer.drawRect(startX-i, startY-i, (endX+i)-(startX-i), (endY+i)-(startY-i));
        }
        drawer.dispose();
    }
}