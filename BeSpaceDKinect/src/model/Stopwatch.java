/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * DEVELOPED BY Alex Yeap 2016
 */

package model;

/*
 * VERY basic class for benchmarking. Just call start() to start and end() to print the time elapsed.
 * NOTE: Every call to start() will restart the counter so counters cannot overlap
 */

public class Stopwatch {
    private Stopwatch(){}

    private static long START = 0;

    public static void start() {
        START = System.currentTimeMillis();
    }
    public static void end() {
        System.out.println((System.currentTimeMillis() - START) / 1000.0 + " seconds");
        START = 0;
    }
}