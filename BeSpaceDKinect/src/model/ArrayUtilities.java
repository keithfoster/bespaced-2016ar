/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * DEVELOPED BY Alex Yeap 2016
 */

package model;

import java.util.List;

public class ArrayUtilities {
    
    /**
     * Expands a 1D array to a 2D array.
     * @param flat The 1D array to convert.
     * @param oneD The height of the 2D array.
     * @param twoD The width of the 2D array.
     * @return The new 2D array.
     */
    public static float[][] expandTo2D(float[] flat, int oneD, int twoD) {
        float[][] expanded = new float[oneD][twoD];

        int counter = 0;
        try
        {
            for(int i=0; i < twoD; i++) {
                for(int j=0; j < oneD; j++) {
                    expanded[j][i] = flat[counter];
                    counter++;
                }
            }
            
            return expanded;
        }
        catch (IndexOutOfBoundsException e) {
            System.out.println(e.getClass() + e.getMessage());
            return null;
        }
    }

    /*
     * zips together any number of arrays such that all first elements are recorded, then all second elements etc.
     * Variable length arrays are safe but no placeholders will be used and arrays won't be truncated
     * ie. if a.length == 3 and b.length == 5, total length of output will be 8, not 10 or even 6.
     */
    public static float[] zipArrays(List<float[]> arrs) {
        float[] output;
        int numArrs = arrs.size();
        int largest = 0;
        int total = 0;

        for (float[] a : arrs) {
            total += a.length;
            if (a.length > largest) largest = a.length;
        }

        output = new float[total];
        // Gets first element of every array, then second, third etc.
        for (int iter = 0, outIter = 0; iter < largest && outIter < total; iter++) {
            for (int arr = 0; arr < numArrs && outIter < total; arr++) {
                float[] curr = arrs.get(arr);
                if (iter < curr.length) {
                    output[outIter++] = curr[iter];
                }
            }
        }

        return output;
    }


    /**
     * Truncates floats to integers. Shift allows for decimal shift (ie. 10 ^ shift)
     * @param vals The floats to be converted.
     * @param shift The decimal shift.
     * @return The resulting integers.
     */
    public static int[] floatsToInts(float[] vals, int shift) {
        int scale = (int) Math.pow(10, shift);
        int[] converted = new int[vals.length];
        try
        {
            for (int i = 0; i < vals.length; i++) {
                converted[i] = (int) (vals[i] * scale);
            }
            
            return converted;
        }
        catch (Exception e) {
            System.out.println(e.getClass() + e.getMessage());
            return null;
        }
    }
}