/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * DEVELOPED BY Alex Yeap 05/2016
 */

package model

import BeSpaceDCore._

object ImportExport {


//    case class Occupy3DPoint (x:Double, y:Double, z:Double) extends ATOM

    // converts array of floats to array of ints, scaled by order of magnitude
    // Needed because Occupy3DPoint etc. requires Int but Kinect outputs metres as floats
    // This may not be safe, it assumes that a -Infinity value for X will also result in -Infinity for Y and Z
    def floatsToInts(floats: Array[Float], order: Int): Array[Int] = {
        for (i <- floats.indices.toArray; if floats(i) != Float.NegativeInfinity) yield {
                (floats(i) * Math.pow(10, order)).toInt
        }
    }

    def bytesToInts(bytes: Array[Byte], order: Int): Array[Int] = {
        for (i <- bytes.indices.toArray) yield {
                (bytes(i) * Math.pow(10, order)).toInt
        }
    }

    // --------------------------------------------------------------------------------- XYZ Values
    
    // Same as import3DSpace but adds time implication for given TimePoint
    def import3DSpaceTime[T](data: Array[Int], timePoint: TimePoint[T]): Invariant =
    {
        return IMPLIES(timePoint, import3DSpace(data))
    }


    // Expects array of format { X, Y, Z, X, Y, ...., Z }
    // Converts to BIGAND(Occupy3DPoint)
    def import3DSpace(data: Array[Int]): Invariant =
    {
        val points = pointsToOccupy3DPoints(data)
        return BIGAND(points)
    }

    
    
    // --------------------------------------------------------------------------------- UV Values
    
    // Same as importUVSpace but adds time implication for given TimePoint
    def importUVSpaceTime[T](data: Array[Int], timePoint: TimePoint[T]): Invariant =
    {
        return IMPLIES(timePoint, importUVSpace(data))
    }


    // Expects array of format { X, Y, X, Y, ...., Y }
    // Converts to BIGAND(OccupyPoint)
    def importUVSpace(data: Array[Int]): Invariant =
    {
        val points = pointsToOccupyUVPoints(data)
        return BIGAND(points)
    }

    
    
    // --------------------------------------------------------------------------------- Color Values
    
    // Same as importColorSpace but adds time implication for given TimePoint
    def importColorSpaceTime[T](data: Array[Int], timePoint: TimePoint[T]): Invariant =
    {
        return IMPLIES(timePoint, importColorSpace(data))
    }


    // Expects array of format { X, Y, X, Y, ...., Y }
    // Converts to BIGAND(OccupyPoint)
    def importColorSpace(data: Array[Int]): Invariant =
    {
        val points = listOfColors(data)
        return BIGAND(points)
    }

    
    
    // --------------------------------------------------------------------------------- All Values
    
    // Same as import3DSpace but adds time implication for given TimePoint
    def importKinectScanTime[T](data: KinectScan, timePoint: TimePoint[T]): Invariant =
    {
        return IMPLIES(timePoint, importKinectScan(data))
    }


    // Expects array of format { X, Y, Z, X, Y, ...., Z }
    // Converts to BIGAND(Occupy3DPoint)
    def importKinectScan(data: KinectScan): Invariant =
    {
        val xyzPoints = pointsToOccupy3DPoints(floatsToInts(data.xyz, 0))
        val uvPoints = pointsToOccupyUVPoints(floatsToInts(data.uv, 0))
        val points = xyzPoints zip uvPoints map { pair => IMPLIES(pair._1, pair._2) }
        val colors = listOfColors(bytesToInts(data.color_data, 0))
        
        // Display some data for documentation purposes
        val somePoints = points take 10
        val someColors = colors take 10
        
        println(s"somePoints = $somePoints")
        println(s"someColors = $someColors")
        
        val pointInvariant = Owner("Points", BIGAND(points))
        val colorInvariant = Owner("Colors", BIGAND(colors))

        return BIGAND(pointInvariant, colorInvariant)
    }

    
    
    // ----------------------------------------------------------------------------------------------------- PRIVATE
    
    // TODO update this to use applyToConjunctionTermList???
    // Creates an Occupy3DBox determined by the min and max X, Y and Z values
    private
    def inferOccupy3DBox(data: Array[Int]): Occupy3DBox = {
        val points = pointsToOccupy3DPoints(data)
        val maxXYZ: Occupy3DPoint = points.reduce((a,b) => comparePoints((c,d) => math.max(c,d))(a,b))
        val minXYZ: Occupy3DPoint = points.reduce((a,b) => comparePoints((c,d) => math.min(c,d))(a,b))

        return Occupy3DBox(maxXYZ.x,maxXYZ.y, maxXYZ.z, minXYZ.x, minXYZ.y, minXYZ.z)
    }


    // Accepts BIGAND (or nested BIGAND) of Occupy3DPoints and converts to flat Array[Int] (ie. java int[])
    private
    // Convert this to use flatten???
    def export3DPoints(inv: Invariant): Array[Int] = {
        def splitTuples(in: List[(Int,Int,Int)]): List[Int] = {
            def iter(in: List[(Int, Int, Int)], out: List[Int]): List[Int] = {
                in match {
                    case Nil => out
                    case ((a,b,c)::tail) => iter(tail, a :: b :: c :: out)
                }

            }
            iter(in, Nil)
        }

        def extract3DPoints(inv: Invariant, lst: List[(Int,Int,Int)]): List[(Int,Int,Int)]  = {
            inv match {
                case BIGAND (Occupy3DPoint(xp,yp,zp)::xs) => extract3DPoints (BIGAND(xs), (xp,yp,zp) :: lst)
                case BIGAND (x::xs) => extract3DPoints (BIGAND(xs), extract3DPoints (x, lst)) // search nested BIGAND
                case _ => lst
            }
        }

        val points: List[(Int,Int,Int)] = extract3DPoints(inv, Nil)
        return splitTuples(points).toArray
    }

    // creates a new Occupy3DPoint from operations performed on equivalent [x|y|z] from two Occupy3DPoints
    private
    def comparePoints(f: (Int,Int) => Int)(a: Occupy3DPoint, b: Occupy3DPoint): Occupy3DPoint = {
        val x = f(a.x, b.x)
        val y = f(a.y, b.y)
        val z = f(a.z, b.z)
        return new Occupy3DPoint(x,y,z)
    }

    // Expects a Seq of format { X, Y, Z, X, Y, Z, X ... , Z }
    private
    def pointsToOccupy3DPoints(data: Array[Int]): List[Occupy3DPoint] = {
        val points: List[Occupy3DPoint] =
            for (i <- data.indices.toList if i % 3 == 2) yield {
                new Occupy3DPoint (data (i - 2), data (i - 1), data (i) )
            }
        return points
    }

    // Expects a Seq of format { X, Y, Z, X, Y, Z, X ... , Z }
    private
    def pointsToOccupyUVPoints(data: Array[Int]): List[ComponentState[(Int,Int)]] = {
        val points: List[ComponentState[(Int,Int)]] =
            for (i <- data.indices.toList if i % 2 == 1) yield {
                new ComponentState[(Int,Int)] ( (data (i - 1), data (i)) )
            }
        return points
    }

    // Expects a Seq of format { R, G, B, R, G, B, R ... , B }
    private
    def listOfColors(data: Array[Int]): List[ComponentState[(Int,Int,Int)]] = 
    {
       val componentValues: List[Int] = data.indices.toList
        val colors: List[ComponentState[(Int, Int, Int)]] =
            for (i: Int <- componentValues if i % 3 == 2) yield 
            {
                ComponentState[(Int,Int,Int)]( (data (i - 2), data (i - 1), data (i)) )
            }
        
        colors
    }


}
