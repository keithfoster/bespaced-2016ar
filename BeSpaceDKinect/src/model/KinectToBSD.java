/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * DEVELOPED BY Alex Yeap 2016
 * 
 * 20 Jul 2016: Keith Foster added code to save a single snapshot as a BeSpceD dataset.
 */

// RUN INSTRUCTIONS:
// To run this main you need the following JVM argument:
//   -Djava.library.path="lib"
 
package model;

import edu.ufl.digitalworlds.j4k.J4KSDK;
import view.ColorPanel;
import view.DepthPanel;

import javax.swing.*;


/*
 * Stripped back version of App to demonstrate direct import from kinect to BeSpaceD
 */
@SuppressWarnings("serial")
public class KinectToBSD {

    private static JFrame colorWindow;
    private static JFrame depthWindow;
    private static ColorPanel colorContent;
    private static DepthPanel depthContent;
    private KinectSensor sensor;
    private FrameProcessor processor;
    private boolean useColor = false;
    private boolean useDepth = false;
    private boolean simulate = false;


    public KinectToBSD() {

        sensor = KinectSensor.getInstance();
        /* Convenience for activating/deactivating windows */
        useColor = true;
        useDepth = true;
//        simulate = true;
//        sensor.setSnapshotMode(true);


        sensor.printDetails();
        processor = FrameProcessor.getInstance();

        if (useDepth) {
            initDepthWindow();
            processor.addObserver(depthContent);
        }

        if (useColor) {
            initColorWindow();
            processor.addObserver(colorContent);
        }

        if (simulate) {
            KinectSimulator.simulateKinect("res/data");
        }
        else {
            sensor.start(J4KSDK.COLOR | J4KSDK.DEPTH | J4KSDK.XYZ | J4KSDK.UV);
        }
    }

    private void initDepthWindow() {
        depthContent = new DepthPanel();
        depthWindow = new JFrame("DEPTH");
        depthWindow.setContentPane(depthContent);
        depthWindow.setSize(527,462);
        depthWindow.setLocation(120,120);
        depthWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        depthWindow.setVisible(true);
    }

    private void initColorWindow() {
        colorContent = new ColorPanel();
        colorWindow = new JFrame("COLOR");
        colorWindow.setContentPane(colorContent);
        colorWindow.setSize(1920,1119);
        colorWindow.setLocation(0,0);
        colorWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        colorWindow.setVisible(true);
    }

    public static void main(String[] args)
    {
    	// NOTE: To run this main you need the following JVM argument:
    	//       -Djava.library.path="lib"
    	
        new KinectToBSD();
    }
}
