/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

import java.io._

import BeSpaceDCore._

package object BeSpaceDData {
  
  object Robotics
  {
    object ABB
    {
      object ARB120
      {
        //def exchangeCanDemo(): Invariant = loadOrThrow("aicause.robotArm.exchangeCan")
      }
    }
    
    object Lego
    {
      object Trains
      {
        def experiment1(): Invariant = loadOrThrow("aicause.lego.trains.experiment1")
      }
      
      object MindStorms
    }
    
    object Festo
    {
      object MiniFactory
      {
        object station1
        {
          def scenario1(): Invariant = loadOrThrow("aicause.festo.station1.Scenario1.20mins")
          def capsBlocking(): Invariant = loadOrThrow("aicause.festo.station1.small.2capsBlocking")

          def live(): Invariant = ??? // TODO: Run the Festo Station 1 Demonstrator live and capture the data.
        }
        
      }
    }
  }
  
  object Weather
  {
    object SmartSpace
    {
      object Melbourne
      {
        def live(): Invariant = ??? // TODO: Run the SmartSpace Demonstrator live and capture the data.
      
        def Aug_27_2015(): Invariant = loadOrThrow("aicause.smartspace.melbourne.2015.aug.27")
        
        def uvIndex_Dec_28_2015(): Invariant = loadOrThrow("aicause.arpansa.2015-12-28")
      }
    }
  }
  
  object Scan
  {
    object Kinect
    {
       def bottle(): Invariant = loadOrThrow("aicause.kinect.scan.bottle")
       def obstacles(): Invariant = loadOrThrow("aicause.kinect.scan.obstacles1a")
    }
    
    object LeapMotion
  }
  
  case class UnknownDatasetException(datasetName: String) extends Exception(s"Unknown Dataset: $datasetName.")
  
  
  
  // BEWARE: Accessing any of these InMemory fields will cause a large amount of data to persist in memory and not be freed
  //         Not recommended for performance reasons.
  //         Better to manage the memory explicitly using the load/release methods below.
  
  object InMemory
  {
    //lazy val robotArmCanExchange = Robotics.ABB.ARB120.exchangeCanDemo()
    
    lazy val legoTrains = Robotics.Lego.Trains.experiment1()
    
    lazy val festoStation1Scenario1 = Robotics.Festo.MiniFactory.station1.scenario1()
    lazy val festoStation1CapsBlocking = Robotics.Festo.MiniFactory.station1.capsBlocking()
    
    lazy val melbourneWeather = Weather.SmartSpace.Melbourne.Aug_27_2015()
    lazy val melbourneUv = Weather.SmartSpace.Melbourne.uvIndex_Dec_28_2015()
    
    lazy val kinectBottle = Scan.Kinect.bottle()
    lazy val kinectObstacles = Scan.Kinect.obstacles()

  }
  
  // -------------------------------------------------------- Storage Management

  // IMPORTANT: Change this to your own path where the data is stored.
  private
  val archivePath = "../data"

  
  // Saving
    
  def save(data: Invariant, name: String)
  {
    val fileOS = new FileOutputStream(s"$archivePath/$name")
    val objectOS = new ObjectOutputStream(fileOS)
    
    objectOS.writeObject(data)
    objectOS.close()
  }
  
  def saveReadable(data: Invariant, name: String)
  {
    val fileOS = new FileOutputStream(s"$archivePath/$name.txt")
    val objectOS = new ObjectOutputStream(fileOS)
    
    objectOS.writeObject(data.toString)
    objectOS.close()
  }
  
  def saveAndCache(data: Invariant, name: String)
  {
    save(data, name)
    addToCache(data, name)
  }
  
  
  
  // Loading

  def load(name: String): Option[Invariant] =
  {
   try
   {
    val fileIS = new FileInputStream(s"$archivePath/$name")
    val objectIS = new ObjectInputStream(fileIS)
    
    Some(objectIS.readObject().asInstanceOf[Invariant]) 
   }
   catch
   {
     case e: FileNotFoundException => None
   }
  }
  
  def loadAndCache(name: String): Option[Invariant] =
  {
    val data = load(name)
    
    if (data.isDefined) 
    { 
      addToCache(data.get, name)
      data
    }
    else 
      None
  }
  
  def findInCache(name: String): Option[Invariant] = cache.get(name)
  
  def findInCacheOrLoad(name: String): Option[Invariant] =
  {
    val data = findInCache(name)
    
    if (data.isEmpty) load(name) else data
  }
  
  def findInCacheOrLoadAndCache(name: String): Option[Invariant] =
  {
    val data = findInCacheOrLoad(name)
    
    if (data.isDefined) addToCache(data.get, name)
    
    data
  }
  
  
  
  // Private values and methods
  
  private
  val cache: Map[String, Invariant] = Map()
  
  private
  def addToCache(data: Invariant, name: String) = cache updated (name, data)

  private
  def loadOrThrow(name: String): Invariant =
  {
    val data = load(name)
    
    if (data.isEmpty) throw UnknownDatasetException(name) else data.get
  }
  
  // TODO: Tech Report
  // API of above
  // show picture of data plus associated bespaced data
  // Just leave Gestault as is. Add a dependency to bsd core and data
  // each App just calls the save(invariant, name) method
  // the default is to just save data on disk as  serialised Invariant objects
  
  
  
// --------------------------------------------------- Test Kinect data set with Double
import scala.compat.Platform.EOL  

def main(args: Array[String])
  {
    val KINECT_OBSTACLES16_ID = "aicause.kinect.scan.obstacles16"
    
    val obstacles16 = loadOrThrow(KINECT_OBSTACLES16_ID)
    
    //println(obstacles16)
    
    assert(obstacles16 match {
      case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => {

        println(s"#Points: ${points.length}")
        println(s"#Points: ${colors.length}$EOL")
        
        println(s"${points slice (10000,10005)}$EOL")
        
        println(colors take 5)
        true
      }
    })
  }

  
  
// --------------------------------------------------- Fix Festo Scenario 1 data set

//  def main(args: Array[String])
//  {
//    val SCENARIO1_ID = "aicause.festo.station1.Scenario1.20mins"
//    
//    val scenario1_old = loadOrThrow(SCENARIO1_ID)
//    val scenario1_fix = fixFestoDataSet(scenario1_old)
//    
//    println(scenario1_fix)
//    
//    save(scenario1_fix, SCENARIO1_ID)
//  }
  
  type EVENT = IMPLIES[AND, ComponentState[Float]]

  def fixFestoDataSet(old: Invariant): Invariant =
  {
      val events: List[EVENT] = (old match { case BIGAND(list) => list }).asInstanceOf[List[EVENT]]
    
      BIGAND(events map fixFestoEvent)
  }
    
  def fixFestoEvent(event: EVENT): EVENT =
  {
      event match {
      
      case IMPLIES(AND(tp, ComponentState(key)), cs) =>
        {  
          IMPLIES(
              AND(tp, Component(key)),
              cs
              )
        }
    }
  }
  
// --------------------------------------------------- Fix Kinect "obstacles1" data set

//  def main(args: Array[String])
//  {
//    val OBSTACLES1_ID = "aicause.kinect.scan.obstacles1"
//    
//    val obstacles1_old = loadOrThrow(OBSTACLES1_ID)
//    val obstacles1_fix = fixKinectScanDataSet(obstacles1_old)
//    
//    println(obstacles1_fix)
//    
//    //save(obstacles1_fix, OBSTACLES1_ID)
//  }
  
  private
  def fixKinectScanDataSet(old: Invariant): Invariant =
  {
      old match {
      
      case IMPLIES(tp, BIGAND(List(Owner(pointsProduct: Product), Owner(colorsProduct: Product)))) =>
        {
          type POINTS = BIGAND[IMPLIES[Occupy3DPoint, ComponentState[(Int,Int)]]]
          type COLORS = BIGAND[ComponentState[(Int,Int,Int)]]
          
          val points: POINTS = pointsProduct.productElement(1).asInstanceOf[POINTS]
          val colors: COLORS = colorsProduct.productElement(1).asInstanceOf[COLORS]
  
          IMPLIES(tp, BIGAND(List(
              IMPLIES(Owner("Points"), points),
              IMPLIES(Owner("Colors"), colors)
              )
          ))
        }
    }
  }
  
}


// ------------------------------------------------------- JAVA COMPATIBILITY

class J
{
  def save(data: Invariant, name: String) = BeSpaceDData.save(data, name)
}



