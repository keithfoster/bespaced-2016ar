
/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/

/* needs runtime setting -Xss515m or similar, Sat4j has problems with number of variables, encodeAsSat uses to low range for encoding of x,y variables */

package SMTOperations
import BeSpaceDCore._;
object ForkliftExample extends CoreDefinitions{

  val networkdescription : Invariant = 
    AND ( AND ( AND ( AND ( AND ( AND(
        Edge ("n2","n3"),
        Edge ("n3","n4")),
        Edge ("n4","n7")),
        Edge ("n3","n6")),
        Edge ("n6","n7")),
        Edge("n1","n4")),
        Edge("n1","n5")); // The existing topological network
  
  val topologicalinvariant_fl1 : Invariant = AND( AND (
      IMPLIES(TimeStamp("pt1"),OccupyNode("n1")), 
      IMPLIES(TimeStamp("pt2"),OR (OccupyNode("n5"),OccupyNode("n4")))), 
      IMPLIES(TimeStamp("pt3"),OR (OccupyNode("n5"),OccupyNode("n7")))); //to be established
  val topologicalinvariant_fl2 : Invariant = AND( AND ( AND (
      IMPLIES(TimeStamp("pt1"),OccupyNode("n2")), 
      IMPLIES(TimeStamp("pt2"),OR (OccupyNode("n3"),OccupyNode("n4")))), 
      IMPLIES(TimeStamp("pt3"),OR (OccupyNode("n6"),OccupyNode("n7")))),
      IMPLIES(TimeStamp("pt4"),OccupyNode("n7")));  //to be established

  
  def invariant1ageneration() : Invariant ={
	var inv1a : Invariant = TRUE();
	  
	for (i <- 0 to 500) {
	  inv1a = AND ((IMPLIES(TimeStamp (i), (OccupyBox(500+(2*i),6000,600+(2*i),6200)))),inv1a);
	}
	for (i <- 501 to 1000) {
	  inv1a = AND ((IMPLIES(TimeStamp (i),  (OccupyBox(500+1000,6000,600+1000,6200) ))),inv1a);
	}
	return inv1a;    
  }
  
  def invariant1asmallgeneration() : Invariant ={
	var inv1a : Invariant = TRUE();
	  
	for (i <- 0 to 500) {
	  inv1a = AND ((IMPLIES(TimeStamp (i), (OccupyBox(500+(2*i),6000,510+(2*i),6001)))),inv1a);
	}
	for (i <- 501 to 1000) {
	  inv1a = AND ((IMPLIES(TimeStamp (i),  (OccupyBox(500+1000,6000,510+1000,6001) ))),inv1a);
	}
	return inv1a;    
  }
  
  def invariant1bgeneration() : Invariant ={
	var inv1b : Invariant = TRUE();
	  
	for (i <- 0 to 500) {
	  inv1b = AND ((IMPLIES(TimeStamp (i),OccupyBox(500+i,6000-i,600+i,6200-i))),inv1b);
	}
	for (i <- 501 to 1000) {
	  inv1b = AND ((IMPLIES(TimeStamp (i), OccupyBox(500+i,6000-i,600+i,6200-i))),inv1b);
	}
	return inv1b;    
  }
  
  def invariant1bsmallgeneration() : Invariant ={
	var inv1b : Invariant = TRUE();
	  
	for (i <- 0 to 500) {
	  inv1b = AND ((IMPLIES(TimeStamp (i),OccupyBox(500+i,6000-i,510+i,6001-i))),inv1b);
	}
	for (i <- 501 to 1000) {
	  inv1b = AND ((IMPLIES(TimeStamp (i), OccupyBox(500+i,6000-i,510+i,6001-i))),inv1b);
	}
	return inv1b;    
  }
  
  
  def invariant1generation() : Invariant ={
    return (OR (invariant1ageneration(),invariant1bgeneration()));
    
    /*
	var inv1 : Invariant = TRUE();
	  
	for (i <- 0 to 500) {
	  inv1 = AND ((IMPLIES(TimeStamp (i), OR (OccupyBox(500+(2*i),6000,600+(2*i),6200) ,OccupyBox(500+i,6000-i,600+i,6200-i)))),inv1);
	}
	for (i <- 501 to 1000) {
	  inv1 = AND ((IMPLIES(TimeStamp (i), OR (OccupyBox(500+(2*i),6000,600+(2*i),6200) ,OccupyBox(500+i,6000-i,600+i,6200-i)))),inv1);
	}
	return inv1;*/
  }

  def invariant2ageneration() : Invariant ={
	var inv2a : Invariant = TRUE();
	  
	for (i <- 0 to 250) {
	  inv2a = AND ((IMPLIES(TimeStamp (i), OccupyBox(500+i,4000+i,600+i,4150+i))),inv2a);
	}
	for (i <- 251 to 500) {
	  inv2a = AND ((IMPLIES(TimeStamp (i), OccupyBox(500+i,4000+250+(2*(i-250)),600+i,4150+250+(2*(i-250))))),inv2a);
	}
	for (i <- 501 to 1000) {
	  inv2a = AND ((IMPLIES(TimeStamp (i), OccupyBox(500+i,4000+750,600+i,4150+750))),inv2a);
	}
	
	return inv2a;
  }
  
  def invariant2bgeneration() : Invariant ={
	var inv2b : Invariant = TRUE();
	  
	for (i <- 0 to 250) {
	  inv2b = AND ((IMPLIES(TimeStamp (i), OccupyBox(500+i,4000+i,600+i,4150+i))),inv2b);
	}
	for (i <- 251 to 500) {
	  inv2b = AND ((IMPLIES(TimeStamp (i), OccupyBox(500+250+(2*(i -250)),4000+250,600+250+(i-250),4150+250))),inv2b);
	}
	for (i <- 501 to 1000) {
	  inv2b = AND ((IMPLIES(TimeStamp (i), OccupyBox(500+250+i,4000+ i - 250,600+250+i,4150+ i - 250))),inv2b);
	}
	return inv2b;
  }
  
  def invariant2generation() : Invariant ={
    return (OR(invariant2ageneration,invariant2bgeneration));
    /*
	var inv2 : Invariant = TRUE();
	  
	for (i <- 0 to 500) {
	  inv2 = AND ((IMPLIES(TimeStamp (i), OccupyBox(500+i,4000+i,600+i,4150+i))),inv2);
	}

	return inv2;*/
  }
  
  def main(args: Array[String]) {
    println("Topological Invariants:");
    println(prettyPrintInvariant(topologicalinvariant_fl1));
    println(prettyPrintInvariant(topologicalinvariant_fl2));
    println ("Geometric Invariants:");
    println(prettyPrintInvariant(this.getSubInvariantForTime(70,simplifyInvariant(invariant1ageneration))));
    println(prettyPrintInvariant(this.getSubInvariantForTime(0,simplifyInvariant(invariant1bgeneration)))); // needs runtime setting -Xss515m or similar
    //println(prettyPrintInvariant(simplifyInvariant(invariant2ageneration)));
    println(prettyPrintInvariant(this.getSubInvariantForTime(643, simplifyInvariant(invariant2ageneration))));
    println(prettyPrintInvariant(this.getSubInvariantForTime(930,simplifyInvariant(invariant2bgeneration))));
        println(prettyPrintInvariant(this.getSubInvariantForTime(931,(invariant2bgeneration))));

        
    println (this.simplifyInvariant( this.unfoldInvariant(getSubInvariantForTime(0,invariant1asmallgeneration()))));    
    println(this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(0,invariant1bsmallgeneration()))));
    println (this.encodeAsSat(this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(0,invariant1bsmallgeneration()))), Nil));
    for (i <- 0 to 1000) {
        //println (this.collisionTest(
        //    this.simplifyInvariant( this.unfoldInvariant(getSubInvariantForTime(i,invariant1asmallgeneration()))), 
        //    this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1bsmallgeneration())))));

        println (this.collisionTestsAlt1( 
            this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1ageneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1bgeneration())))::Nil, 
            this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2bgeneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2ageneration())))::Nil));

    	//println (this.collisionTests( getSubInvariantForTime(i,invariant2bgeneration()) :: Nil, getSubInvariantForTime(i,invariant2bgeneration())::Nil));
    }
  }
}