
/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/

package SMTOperations

import com.microsoft.z3._;
//import java.util.HashMap;
import BeSpaceDCore._;
//import examples.ForkliftExample;
//import invariants._;


object SMTTest extends CoreOperations {

  def timetests1 : Boolean ={
    val cdefs = new CoreDefinitions();
    var retval = false;
    for (i<- 900 to 999) {
      // here we only compare two invariants
      (cdefs.simplifyInvariant((cdefs.getSubInvariantForTime(i,ForkliftExample.invariant1ageneration()))), cdefs.simplifyInvariant((cdefs.getSubInvariantForTime(i,ForkliftExample.invariant2ageneration())))) match  {
        case (OccupyBox (c1x1,c1y1,c1x2,c1y2) ,OccupyBox (c2x1,c2y1,c2x2,c2y2)) => retval |= check2BoxSMT(c1x1,c1y1,c1x2,c1y2,c2x1,c2y1,c2x2,c2y2);
        case (i1,i2) => println("Error");
      }
     } 
    return retval;
  }
  
  def main(args : Array[String]) {
    
    val cdefs = new CoreDefinitions();
    
    println ("SMT test");
    
    
    println (check2BoxSMT(100,110,100,110,100,110,100,110));
    
    
    ForkliftExample.invariant1ageneration;
    println("Tests 1");
    var time = System.currentTimeMillis();
    //Experiments 1 and 2
    //println (timetests1);
    //println (timetests1);
    //println (timetests1);
    //println (timetests1);
    //println (timetests1);
    //println (timetests1);
    //println (timetests1);
    //println (timetests1);
    //println (timetests1);
    //println (timetests1);
    
    //experiment 3
    var l1 : List[Invariant] = Nil;
    var l2 : List[Invariant] = Nil;

    for (i<- 0 to 999) {
      // here we only compare two invariants
      (cdefs.simplifyInvariant((cdefs.getSubInvariantForTime(i,ForkliftExample.invariant1ageneration()))), cdefs.simplifyInvariant((cdefs.getSubInvariantForTime(i,ForkliftExample.invariant2bgeneration())))) match  {
        case (OccupyBox (c1x1,c1y1,c1x2,c1y2) ,OccupyBox (c2x1,c2y1,c2x2,c2y2)) => l1 ::= OccupyBox (c1x1,c1y1,c1x2,c1y2); l2 ::= OccupyBox (c2x1,c2y1,c2x2,c2y2);
        case (i1,i2) => println("Error");
      }
     } 
    println(check2BoxListSMT(l1,l2));
    println("Time needed :"+ (System.currentTimeMillis() - time) + "milliseconds");
    
    
    println("Some checks");
    time = System.currentTimeMillis();
    for (i<- 900 to 999) {
    	        cdefs.collisionTestsAlt1( 
        		     cdefs.simplifyInvariant(cdefs.unfoldInvariant(cdefs.getSubInvariantForTime(i,ForkliftExample.invariant1ageneration())))::Nil,
        		     cdefs.simplifyInvariant(cdefs.unfoldInvariant(cdefs.getSubInvariantForTime(i,ForkliftExample.invariant2ageneration())))::Nil);
    }
    println("Time needed :"+ (System.currentTimeMillis() - time) + "milliseconds");

    println("Some more checks");
    for (i<- 900 to 901) {
      // here we only compare two invariants
      (cdefs.simplifyInvariant((cdefs.getSubInvariantForTime(i,ForkliftExample.invariant1ageneration()))), cdefs.simplifyInvariant((cdefs.getSubInvariantForTime(i,ForkliftExample.invariant2ageneration())))) match  {
        case (OccupyBox (c1x1,c1y1,c1x2,c1y2) ,OccupyBox (c2x1,c2y1,c2x2,c2y2)) => println(check2BoxSMT(c1x1,c1y1,c1x2,c1y2,c2x1,c2y1,c2x2,c2y2));
        case (i1,i2) => println("Error");
      }
      
          println("Next Test:");

      //here we compare 4 invariants
         println (
        		 cdefs.collisionTestsAlt1( 
        		     cdefs.simplifyInvariant(cdefs.unfoldInvariant(cdefs.getSubInvariantForTime(i,ForkliftExample.invariant1ageneration()))):: 
        		     cdefs.simplifyInvariant(cdefs.unfoldInvariant(cdefs.getSubInvariantForTime(i,ForkliftExample.invariant1bgeneration())))::Nil,
        		     cdefs.simplifyInvariant(cdefs.unfoldInvariant(cdefs.getSubInvariantForTime(i,ForkliftExample.invariant2ageneration()))):: 
        		     cdefs.simplifyInvariant(cdefs.unfoldInvariant(cdefs.getSubInvariantForTime(i,ForkliftExample.invariant2bgeneration())))::Nil));
    }
    
    
    println("Next Test:");
    val context :Context  = new Context();
    
    val x = context.mkSymbol("x");
    val y = context.mkSymbol("y");
    //val b = context.mkImplies(
    //				context.mkEq(x,y),
    //                context.mkEq(dereffun.Apply(pconst), so.applyValue(obconst)));
    //context.MkAnd(new IntExpr(),"x");
  
    //val a1 = context.mkInt(22);
    //val a2 = context.mkInt(23);    
    //val a3 = context.mkIntConst(x);
    //val a4 = context.mkIntConst(y);
    
    val xconst = context.mkIntConst(x);
    val yconst = context.mkIntConst(y);

    //val gteq1 = context.mkGt(a1,a3);
    //val gteq2 = context.mkGt(a3,a4);
    
    val maincond = (
    		context.mkAnd(context.mkLe(context.mkInt(10),xconst), context.mkLe(xconst,context.mkInt(20)),
    		context.mkLe(context.mkInt(25),xconst), context.mkLe(xconst,context.mkInt(35)),
    		context.mkLe(context.mkInt(10),yconst), context.mkLe(yconst,context.mkInt(20)),
    		context.mkLe(context.mkInt(15),yconst), context.mkLe(yconst,context.mkInt(25))
    		));

    try {
       val s = context.mkSolver();

      s.add(maincond);

      val result = s.check();
      println(s.getModel().toString());

    } catch {
      case e : Z3Exception => println ("OK " + e);
    }
    
    
    
  }
}