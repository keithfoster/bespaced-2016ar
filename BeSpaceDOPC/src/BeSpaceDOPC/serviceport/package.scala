package BeSpaceDOPC

/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

import java.io.File
import java.util.Date
import java.text.SimpleDateFormat
import java.lang.IllegalArgumentException

import BeSpaceDCore._
import BeSpaceDCore.Log._

import BeSpaceDOPC.serviceport._


/**
 * @author keith
 */
package object serviceport {
  
//  type DataLogLine = List[Float]
//  type DataLog = List[DataLogLine]
  

  
  // -------------------------------------------------------------------- Types

  type TransformElement = (List[String], Int, String) => Invariant
  type TransformSample = (List[String], List[PARSERTOKEN]) => Invariant
  type ParseElement = (List[String], Int, String) => PARSERTOKEN

  
  // -------------------------------------------------------------------- Datafile Parser
  
  
  /**
   * NAIEVE Parsing of datafile : element bu element
   */
  def parseDatafile(filename: String, transform: TransformElement): Unit = 
  {
    val dataFile = new File(filename)
    val data = Datafile(dataFile)
    
    // Header rows etc.
    val expectedLine = data.expectLine("datafile")
    println(s"EXPECTED: $expectedLine")
    
    val columns = data.headerRow
    val columnTypes = columns map { name: String => name.substring(name.lastIndexOf(".")+1) }
    println(s"COLUMN TYPES: $columnTypes")

    val ignoredLine = data.ignoreLine
    println(s"IGNORED: $ignoredLine")
    
    // process the data rows as a stream
    val specificElementProcessor = { (i: Int, e: String) => transform(columnTypes, i, e)}
    
    while (data.dataLoggerRowSimple( specificElementProcessor ).isDefined) {}
    
  }
  
 
  /**
   * More ADVANCED parsing of datafile: row by row and tokenizing the elements first.
   * This allows us to generate more complex BeSPaceD invariants that aggregate information from the whole row.
   */
  def parseDatafile2(filename: String, parseElement: ParseElement, transformSample: TransformSample): Unit =
  {
    val dataFile = new File(filename)
    val data = Datafile(dataFile)
    
    // Header rows etc.
    val expectedLine = data.expectLine("datafile")
    println(s"EXPECTED: $expectedLine")
    
    val columns = data.headerRow

    val ignoredLine = data.ignoreLine
    println(s"IGNORED: $ignoredLine")
    
    
    
    // FIRST, Create a parser for converting the raw rows into tokens (embedding control loop type information)
    val columnTypes = columns map { name: String => name.substring(name.lastIndexOf(".")+1) }
    println(s"COLUMN TYPES: $columnTypes")

    def specificRowParser(rawRow: List[String]): List[PARSERTOKEN] =
      {
        val indexedRawRow: List[(String, Int)] = rawRow zip (0 until rawRow.length)
        
        indexedRawRow map { t: (String, Int) => parseElement(columnTypes, t._2, t._1) }
      }
    
    
    
    // SECOND, create a transformer from a row of tokens to an invariant
    val columnNames = columns map {
      name: String =>
        val dotPosition = name.lastIndexOf(".")
        val endOfName = dotPosition match
        {
          case -1 => name.length
          case _  => dotPosition
        }

        name.substring(0, endOfName)
    }
    println(s"COLUMN NAMES: $columnNames")
    
    def specificSampleTransformer(tokenizedRow: List[PARSERTOKEN]): Invariant =
    {
        val result = transformSample(columnNames, tokenizedRow)
        
        testOn
        {
          new CoreDefinitions().prettyPrintInvariant(result)
        }
        
        result
    }
    
    
    
    // THIRD, create a combined row transformer from raw rows to an invariant.

    def specificSampleParserAndTransformer: List[String] => Invariant = specificSampleTransformer _ compose  specificRowParser
    
    
    
    // FOURTH, process the raw rows as a stream

    while (data.dataLoggerRow( specificSampleParserAndTransformer ).isDefined) {}

  }
  
  
  
  // -------------------------------------------------------------------- Datafile Processor

  /**
   * This is only an example ServicePort Datafile --> BeSpaceD Invariant mapping.
   * Others are possible.
   * 
   * This converts one element at a time - which has limited use.
   */
  def dataLoggerElementTransformer(columnTypes: List[String], index: Int, element: String): Invariant =
  {
    if (index == 0)
    {
      // Time
      
      // Note: Example Time Syntax: 4/27/2015 6:00:02 PM
      val DataLoggerDateFormat = "dd/MM/yyyy hh:mm:ss a"
      val formatter: SimpleDateFormat = new SimpleDateFormat(DataLoggerDateFormat)
      
      val time: Date = formatter.parse(element)
      
      TimePoint(time)
    }
    else 
    {
      val variableType = columnTypes(index)
      
      variableType match
        {
        case "MV" => // Measurement
          {
            ComponentState(element.toFloat)
          }
        case "SP" => // Set Point
          {
            ComponentState(element.toFloat)
          }
        case "OUT" => // Output
          {
            ComponentState(element.toFloat)
          }
        case "AUTO" => // Auto
          {
            ComponentState(element.toInt)
          }
        case _ => throw new IllegalArgumentException(s"Invalid variable type: '$variableType'")
        }
    }
  }


  
    // Variable Types
  
  abstract class OwnerType(unitType : String)
  case class T_MEASUREMENT (unitType : String = "") extends OwnerType(unitType) { override def toString = s"Measurement${unitDescription(unitType)}" }
  case class   T_SET_POINT (unitType : String = "") extends OwnerType(unitType) { override def toString = s"Set Point${unitDescription(unitType)}" }
  case class      T_OUTPUT (unitType : String = "") extends OwnerType(unitType) { override def toString = s"Output${unitDescription(unitType)}" }
  case class        T_AUTO (unitType : String = "") extends OwnerType(unitType) { override def toString = s"Auto${unitDescription(unitType)}" }
  
  def unitDescription(unitType: String): String = if (unitType == "") "" else s"in $unitType"
  
  abstract class PARSERTOKEN
  case class  SampleTime(value: Date)  extends PARSERTOKEN
  case class Measurement(value: Float) extends PARSERTOKEN
  case class    SetPoint(value: Float) extends PARSERTOKEN
  case class      Output(value: Float) extends PARSERTOKEN
  case class        Auto(value: Int)   extends PARSERTOKEN


  /**
   * This is only an example ServicePort Datafile --> BeSpaceD Invariant mapping.
   * Others are possible.
   * 
   * This converts one row at a time - which allows invariants to to use data from any element.
   */
  def dataLoggerSampleTransformer(columnNames: List[String], row: List[PARSERTOKEN]): Invariant =
  {
      def convertList(namedList: List[(String, PARSERTOKEN)]): Invariant =
      {        
        namedList match
        {
          case Nil => TRUE()
          
          case (n,  st:  SampleTime   ) :: tail => IMPLIES(TimePoint(st.value), convertList(tail))
          case (n,      Measurement(v)) :: tail => AND(IMPLIES(AND(Owner(n), Owner(T_MEASUREMENT())), ComponentState(v)), convertList(tail))
          case (n,         SetPoint(v)) :: tail => AND(IMPLIES(AND(Owner(n), Owner(T_SET_POINT())),   ComponentState(v)), convertList(tail))
          case (n,           Output(v)) :: tail => AND(IMPLIES(AND(Owner(n), Owner(T_OUTPUT())),      ComponentState(v)), convertList(tail))
          case (n,             Auto(v)) :: tail => AND(IMPLIES(AND(Owner(n), Owner(T_AUTO())),        ComponentState(v)), convertList(tail))
          
          case _ => FALSE()
        }
        
      }

    val namedRow = columnNames zip row
    
    new CoreDefinitions().simplify(convertList(namedRow))
  }



  def dataLoggerElementParser(columnTypes: List[String], index: Int, element: String): PARSERTOKEN =
  {
    if (index == 0)
    {
      // Time
      
      // Note: Example Time Syntax: 4/27/2015 6:00:02 PM
      val DataLoggerDateFormat = "dd/MM/yyyy hh:mm:ss a"
      val formatter: SimpleDateFormat = new SimpleDateFormat(DataLoggerDateFormat)
      
      val time: Date = formatter.parse(element)
      
      SampleTime(time)
    }
    else 
    {
      val variableType = columnTypes(index)
      
      variableType match
        {
        case "MV" => // Measurement
          {
            Measurement(element.toFloat)
          }
        case "SP" => // Set Point
          {
            SetPoint(element.toFloat)
          }
        case "OUT" => // Output
          {
            Output(element.toFloat)
          }
        case "AUTO" => // Auto
          {
            Auto(element.toInt)
          }
        case _ => throw new IllegalArgumentException(s"Invalid variable type: '$variableType'")
        }
    }
  }
  
}