/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Locale;

import org.opcfoundation.ua.builtintypes.DataValue;
import org.opcfoundation.ua.builtintypes.LocalizedText;
import org.opcfoundation.ua.builtintypes.ExpandedNodeId;
import org.opcfoundation.ua.builtintypes.NodeId;
import org.opcfoundation.ua.builtintypes.UnsignedInteger;
import org.opcfoundation.ua.common.ServiceResultException;
import org.opcfoundation.ua.core.ApplicationDescription;
import org.opcfoundation.ua.core.ApplicationType;
import org.opcfoundation.ua.core.Attributes;
import org.opcfoundation.ua.core.Identifiers;
import org.opcfoundation.ua.core.ReferenceDescription;
import org.opcfoundation.ua.transport.security.SecurityMode;
import org.opcfoundation.ua.utils.AttributesUtil;

import com.prosysopc.ua.ApplicationIdentity;
import com.prosysopc.ua.SecureIdentityException;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.AddressSpace;
import com.prosysopc.ua.client.AddressSpaceException;
import com.prosysopc.ua.client.ServerConnectionException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.nodes.UaNode;
import com.prosysopc.ua.nodes.UaReferenceType;

/**
 * A very minimal client application. Connects to the server and reads one
 * variable. Works with a non-secure connection.
 */
public class MatrikonUAClientToDAServerViaGateway {
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception 
	{
		// Connection
		
		// OPC DA Matrikon Server via UA Gateway
		UaClient client = new UaClient("opc.tcp://10.234.2.208:4850/Matrikon.OPC.Simulation.1");
		
		client.setSecurityMode(SecurityMode.NONE);
		initialize(client);
		client.connect();
		
		// Server State
		System.out.println("Server status...");
		DataValue value = client.readValue(Identifiers.Server_ServerStatus_State);
		System.out.println(value);
		
		// Address Space
		AddressSpace addressSpace = client.getAddressSpace();
		List<ReferenceDescription> references = addressSpace.browse(Identifiers.RootFolder);
		for (int i = 0; i < references.size(); i++)
			{
			System.out.println(i + " - " + referenceToString(client , references.get(i)));
			}
		
		// Read a Value
		// TODO:

		// Objects
		Integer action = 1;

		ReferenceDescription r = references.get(action);
		ExpandedNodeId expandedNodeId = r.getNodeId();
		NodeId id4Objects = client.getAddressSpace().getNamespaceTable().toNodeId(expandedNodeId);

		System.out.println("id4Objects: " + id4Objects);
		
		
		// Folder
		references = client.getAddressSpace().browse(id4Objects);
		action = 2;

		r = references.get(action);
		expandedNodeId = r.getNodeId();
		NodeId id4Folder = client.getAddressSpace().getNamespaceTable().toNodeId(expandedNodeId);

		System.out.println("id4Folder: " + id4Folder);
		
				
		
		// Write a value
		for (int i=0; i < 10; i++)
		{
		   Boolean done = client.writeAttribute(id4Folder, Attributes.Value, 7 * i);
		   
		   System.out.println("done: " + done);
		   Thread.sleep(1000);
		}
		
		client.disconnect();
	}
	
	


	/**
	 * @param r
	 * @return
	 * @throws ServiceException
	 * @throws ServerConnectionException
	 * @throws StatusException
	 */
	protected static String referenceToString(UaClient client, ReferenceDescription r)
			throws ServerConnectionException, ServiceException, StatusException {
		if (r == null)
			return "";
		String referenceTypeStr = null;
		try {
			// Find the reference type from the NodeCache
			UaReferenceType referenceType = (UaReferenceType) client.getAddressSpace().getType(r.getReferenceTypeId());
			if ((referenceType != null) && (referenceType.getDisplayName() != null))
				if (r.getIsForward())
					referenceTypeStr = referenceType.getDisplayName().getText();
				else
					referenceTypeStr = referenceType.getInverseName().getText();
		} catch (AddressSpaceException e) {
			System.out.println(e);
			System.out.println(r.toString());
			referenceTypeStr = r.getReferenceTypeId().getValue().toString();
		}
		String typeStr;
		switch (r.getNodeClass()) {
		case Object:
		case Variable:
			try {
				// Find the type from the NodeCache
				UaNode type = client.getAddressSpace().getNode(r.getTypeDefinition());
				if (type != null)
					typeStr = type.getDisplayName().getText();
				else
					typeStr = r.getTypeDefinition().getValue().toString();
			} catch (AddressSpaceException e) {
				System.out.println(e);
				System.out.println("type not found: " + r.getTypeDefinition().toString());
				typeStr = r.getTypeDefinition().getValue().toString();
			}
			break;
		default:
			typeStr = "[" + r.getNodeClass() + "]";
			break;
		}
		return String.format("%s%s (ReferenceType=%s, BrowseName=%s%s)", r.getDisplayName().getText(), ": " + typeStr,
				referenceTypeStr, r.getBrowseName(), r.getIsForward() ? "" : " [Inverse]");
	}


	/**
	 * Define a minimal ApplicationIdentity. If you use secure connections, you
	 * will also need to define the application instance certificate and manage
	 * server certificates. See the SampleConsoleClient.initialize() for a full
	 * example of that.
	 */
	protected static void initialize(UaClient client)
			throws SecureIdentityException, IOException, UnknownHostException {
		// *** Application Description is sent to the server
		ApplicationDescription appDescription = new ApplicationDescription();
		appDescription.setApplicationName(new LocalizedText("SimpleClient", Locale.ENGLISH));
		// 'localhost' (all lower case) in the URI is converted to the actual
		// host name of the computer in which the application is run
		appDescription.setApplicationUri("urn:localhost:UA:SimpleClient");
		appDescription.setProductUri("urn:prosysopc.com:UA:SimpleClient");
		appDescription.setApplicationType(ApplicationType.Client);

		final ApplicationIdentity identity = new ApplicationIdentity();
		identity.setApplicationDescription(appDescription);
		client.setApplicationIdentity(identity);
	}

}
