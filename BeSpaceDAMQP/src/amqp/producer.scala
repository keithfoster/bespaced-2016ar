package amqp

import BeSpaceDCore._

object AMQPproducer extends CoreDefinitions
{
  
  val someData = BIGAND ((OccupyBox(100,140,1100,160))::Nil)
  val someSensor = TimePoint()
  
  def main(args: Array[String]): Unit = {
    println(someData.toString())
  }
  
  def getString(): String = {
    someData.toString()
  }
}