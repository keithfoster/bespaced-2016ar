/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto

class FestoData {
  
    def pinToDevice(pin: String): String =
    {
      pin match {
        case "ejectionAirPulseSol" => "Ejection Air Pulse SOL"  // GPIO_06"
        case "loaderPickupSol" => "Loader Pickup SOL"  // <GPIO 26>
        case p => p
      }
    }
    
    def stateToValue(device: String)(state: String): Float = 
    {
    (device, state) match {
            case (_, "HIGH") => 101.2F 
            case (_, "LOW") => 88.4F
        }
    }

}