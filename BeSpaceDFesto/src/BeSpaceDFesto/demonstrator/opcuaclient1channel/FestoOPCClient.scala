/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.demonstrator.opcuaclient1channel

import java.util.Date
import BeSpaceDCore._

// OPC
import BeSpaceDOPC._
import com.prosysopc.ua.client.UaClient
import BeSpaceDOPC.demonstrator.WeatherDataTestUAClientToUAServerViaGateway._    
// HACK: Please refactor the OPC code to extract reusable utilities from the demonstrator



object FestoOPCClient {
  
   var history: BIGAND[Invariant] = BIGAND(Nil); 
   

   val client: UaClient = connect()
   val fc1001 = navigateToFc1001(client)
   val setpoint = fc1001.browse(SETPOINT)
   setpoint.print

   var sessionName: String = "None"
  
    def beginSession(sessionName: String)
    {
      this.sessionName = sessionName
      
      println()
      println()
      println()
      
      println(s"==================== BEGIN OPC SESSION: $sessionName")
    }
   
   
    def endSession
    {
      println(s"==================== END OPC SESSION: $sessionName")
      
      println()
      println()
      println()
      
      logHistory()
    }
   
   
   
    def importSignal(festoDevice: String, festoValue: Float) 
    {
      // 1. Create the signal in a BeSpaceD representation.
      val signal = IMPLIES(AND(TimePoint[Date](new Date()), ComponentState(festoDevice)), ComponentState(festoValue))
      
      println(s"New Signal to import: $signal")
      
      
      // 2. Store this signal in the history.
      history = BIGAND(signal :: history.terms)
      
      
      // 3. Write this signal to the Sample OPC UA server running in the GovLab
      setpoint.writeValue(festoValue)
      
      //logHistory()
    }
    
    def logHistory() = println(history)
}