/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model

import BeSpaceDCore._
import PhysicalModel._



class FestoPart(name: String) extends Part[ID](name)
object FestoPart { def apply(name:String): FestoPart = { new FestoPart(name) } }

object Parts {
  
    // Station 1
    val CapTube       = FestoPart("Cap Tube")
    val StackEjector  = FestoPart("Stack Ejector")
    val CapLoader     = FestoPart("Cap Loader")
    val VacuumGripper = FestoPart("Vacuum Gripper")
        
    // TODO...
    // Station 2
    // Station 3
    // Station 4
    // Station 5
    // Station 6
    // Station 7
    // Station 8
    // Station 9
    // Station 10

  type FestoPartComponent  = Component[FestoPart]
  type FestoPartOccupyNode = OccupyNode[FestoPart]
  
}