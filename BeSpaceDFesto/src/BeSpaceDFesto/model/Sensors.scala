/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model

import BeSpaceDCore._
import PhysicalModel._



class FestoSensor(name:String) extends Sensor[ID](name)
object FestoSensor { def apply(name:String): FestoSensor = { new FestoSensor(name) } }

object Sensors {

    // Station 1
    val StackEjectorExtended  = FestoSensor("Stack Ejector Extended" )
    val StackEjectorRetracted = FestoSensor("Stack Ejector Retracted")
    val WorkpieceGripped      = FestoSensor("Workpiece Gripped"      )
    val LoaderPickedUp        = FestoSensor("Loader Picked Up"       )
    val LoaderDroppedOff      = FestoSensor("Loader Dropped Off"     )
    val StackEmpty            = FestoSensor("Stack Empty"            )
    
    // TODO...
    // Station 2
    // Station 3
    // Station 4
    // Station 5
    // Station 6
    // Station 7
    // Station 8
    // Station 9
    // Station 10

                        
  type FestoSensorComponent  = Component[FestoSensor]
  type FestoSensorOccupyNode = OccupyNode[FestoSensor]  
  
}

object SensorsInState
{
  import Sensors._
  
    val  StackEjectorRetractedOn = StackEjectorRetracted ==> Obstructed
    val StackEjectorRetractedOff = StackEjectorRetracted ==> Unobstructed
    val   StackEjectorExtendedOn = StackEjectorExtended  ==> Obstructed
    val  StackEjectorExtendedOff = StackEjectorExtended  ==> Unobstructed
    val             StackEmptyOn = StackEmpty            ==> Obstructed
    val            StackEmptyOff = StackEmpty            ==> Unobstructed
    val       WorkpieceGrippedOn = WorkpieceGripped      ==> Obstructed
    val      WorkpieceGrippedOff = WorkpieceGripped      ==> Unobstructed
    val         LoaderPickedUpOn = LoaderPickedUp        ==> Obstructed
    val        LoaderPickedUpOff = LoaderPickedUp        ==> Unobstructed
    val       LoaderDroppedOffOn = LoaderDroppedOff      ==> Obstructed
    val      LoaderDroppedOffOff = LoaderDroppedOff      ==> Unobstructed
    
    type FestoSensorInState = IMPLIES[FestoSensor, FestoSensorState]

}


