/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model

import BeSpaceDCore._

import Parts._
import Actuators._
import Sensors._



// TODO: Review this first topology attemp and improve it to use the stateful topology
//       Also fix the repetition (copy/paste)

object FactoryTopology {
  
  val core = standardDefinitions; import core._

  // NOTES:
  //        The Festo mini factory topology is specified using a graph.
  //        This graph is represented in BeSPaceD as nodes and edges.
  
  
  
  // ------------------------------------------------------------------ NODES
  
  // The nodes of the graph is represented with the device identity components.
  // (see the model package object)
  //
  // e.g. OccupyNode(StackEjectorExtend)
  
  
  // ------------------------------------------------------------------ EDGES
  
  // The edges of the graph are represented with a Connection invariant.
  //
  // e.g. Connection(StackEjectorExtend, VacuumGripper)
  
  type Topology         = BeGraph[OccupyNode[FestoDevice]  ]
  type PartTopology     = BeGraph[OccupyNode[FestoPart]    ]
  type ActuatorTopology = BeGraph[OccupyNode[FestoActuator]]
  type SensorTopology   = BeGraph[OccupyNode[FestoSensor]  ]
  
  val partTopology: PartTopology = BeGraph(
                 OccupyNode(StackEjector) --> OccupyNode(VacuumGripper) ^
                 OccupyNode(StackEjector) --> OccupyNode(VacuumGripper) ^
                 OccupyNode(StackEjector) --> OccupyNode(VacuumGripper)
                 )

  val actuatorTopology: ActuatorTopology = BeGraph(
                 OccupyNode(StackEjectorExtend) --> OccupyNode(VacuumGrip) ^
                 OccupyNode(StackEjectorExtend) --> OccupyNode(VacuumGrip) ^
                 OccupyNode(StackEjectorExtend) --> OccupyNode(VacuumGrip)
                 )

  val sensorTopology: SensorTopology = BeGraph(
                 OccupyNode(StackEjectorExtended) --> OccupyNode(StackEjectorRetracted) ^
                 OccupyNode(StackEjectorExtended) --> OccupyNode(StackEjectorRetracted) ^
                 OccupyNode(StackEjectorExtended) --> OccupyNode(StackEjectorRetracted)
                 )

 //val topology: Topology = partTopology ++ actuatorTopology ++ sensorTopology
                 
                 
  // -------------- TYPING ALTERNATIVES
                 
  // Alternative 1: Using BeMaps of OccupyNode
  
  type TopologyBM         = BeMap[OccupyNode[FestoDevice],   OccupyNode[FestoDevice]  ]
  type PartTopologyBM     = BeMap[OccupyNode[FestoPart],     OccupyNode[FestoPart]    ]
  type ActuatorTopologyBM = BeMap[OccupyNode[FestoActuator], OccupyNode[FestoActuator]]
  type SensorTopologyBM   = BeMap[OccupyNode[FestoSensor],   OccupyNode[FestoSensor]  ]
  
  val sensorTopologyBM: SensorTopologyBM = BeMap(
                 OccupyNode(StackEjectorExtended) ==> OccupyNode(StackEjectorRetracted) ^
                 OccupyNode(StackEjectorExtended) ==> OccupyNode(StackEjectorRetracted) ^
                 OccupyNode(StackEjectorExtended) ==> OccupyNode(StackEjectorRetracted)
                 )

                 
  // Alternative 2: Using BIGAND and Edge of OccupyNode

  type TopologyBE         = BIGAND[Edge[OccupyNode[FestoDevice]  ]]
  type PartTopologyBE     = BIGAND[Edge[OccupyNode[FestoPart]    ]]
  type ActuatorTopologyBE = BIGAND[Edge[OccupyNode[FestoActuator]]]
  type SensorTopologyBE   = BIGAND[Edge[OccupyNode[FestoSensor]  ]]
  
  val sensorTopologyBe: SensorTopologyBE =
                 OccupyNode(StackEjectorExtended) --> OccupyNode(StackEjectorRetracted) ^
                 OccupyNode(StackEjectorExtended) --> OccupyNode(StackEjectorRetracted) ^
                 OccupyNode(StackEjectorExtended) --> OccupyNode(StackEjectorRetracted)
                  
  }


